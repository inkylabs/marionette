/*
 *   Copyright 2011 Inkylabs et al.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License")
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * <Insert Marionetastical limerick here>
 *
 */


/**
 * Replay is a wrapper around the Game class that we pass to jaws.
 *
 * canvasId:
 *   {String} The canvas id for the world
 */
replay.Replay = function(div_id, replay_url, media_prefix, bust_cache) {
    // Initialize some variables
    this.bust_cache = bust_cache
    this.curround = 0
    this.meshes = {}
    this.players = {}
    this.roundtime = 100;
    this.lastroundinc = 0;

    // Make sure we can use webgl
    // TODO: Make this message prettier
    if (!replay.has_webgl()) {
        alert('You need WebGL!')
    }

    // Begin the round assembler
    this.assemble_rounds(replay_url)

    var container = $('#' + div_id)

    this.camera = new THREE.PerspectiveCamera(
            40, window.innerWidth / window.innerHeight, 1, 10000)
    this.camera.position.z = 1800

    this.scene = new THREE.Scene()
    this.scene.add(this.camera)

    var light = new THREE.DirectionalLight( 0xffffff )
    light.position.set( 0, 0, 1 ).normalize()
    this.scene.add(light)

    var loader = new THREE.JSONLoader()

    this.renderer = new THREE.WebGLRenderer({antialias: true})
    this.renderer.setSize(window.innerWidth, window.innerHeight)

    container.append(this.renderer.domElement)

    // Add the stats element
    // TODO: Only do this in development mode
    var stats = new Stats()
    stats.domElement.style.position = 'absolute'
    stats.domElement.style.top = '0px'
    container.append(stats.domElement)

    // Load the models and begin the animation
    var that = this
    replay.loadModels(function() {
        var animate = function() {
            requestAnimationFrame(animate)
            that.render()
            stats.update()
        }
        animate()
    })
}


/**
 * Asynchronous function to assemble the rounds of the replay
 * from several different files.
 */
replay.Replay.prototype.assemble_rounds = function(replay_url) {
    // The cumulative number of rounds prior to each file
    var cumulativeRounds = [0]
    this.rounds = []
    var that = this
    $.doTimeout('round_assembler', 5000, function() {
        // If we are finished, go ahead and exit
        if (that.rounds.length > 0 &&
            that.rounds[that.rounds.length - 1].end === true) {
            return false
        }

        // Construct the url
        var curfilenum = cumulativeRounds.length - 1
        var url = replay_url + '/' + curfilenum
        if (that.bust_cache) {
            url += '?' + new Date().getTime()
        }

        // Get the file
        $.getJSON(url, function(data) {
            var prevRounds = cumulativeRounds[curfilenum]
            var totalRounds = prevRounds + data.length
            cumulativeRounds[curfilenum + 1] = totalRounds
            // We insert these directly instead of just calling concat to
            // handle the case where this function gets called twice with
            // the same curfilenum
            for (var i = 0; i < data.length; i++) {
                that.rounds[prevRounds + i] = data[i]
            }
        })
        return true
    })
    // Start the round assembler immediately
    $.doTimeout('round_assembler', true)
}


replay.Replay.prototype.handle_new_player = function(key, change) {
    this.players[key] = new replay.Player(change)
}


replay.Replay.prototype.handle_new_mesh = function(key, change) {
    var change = $.extend({round: this.curround}, change)
    this.meshes[key] = new replay.Mesh(change)
    this.scene.add(this.meshes[key])
}


replay.Replay.prototype.render = function() {
    // Wait on more rounds if we need to
    if (!this.rounds[this.curround] && !this.gameover) {
        //this.loadingdiv.show()
        return
    }
    //this.loadingdiv.hide()

    // Get changes
    var timestamp = new Date().getTime()
    var timefrac = (timestamp - this.lastroundinc) / this.roundtime
    var incround = false
    var changes = {}
    if (timefrac > 1.0) {
        incround = true
        this.lastroundinc = timestamp
        timefrac = 0.0
        if (!this.gameover) {
            changes = this.rounds[this.curround]
        }
    }

    // Handle changes in this round
    var mode_order = [
        [replay.is_player, 'handle_new_player', this.players],
        [replay.is_mesh, 'handle_new_mesh', this.meshes],
    ]
    for (var i = 0; i < mode_order.length; i++) {
        var test = mode_order[i][0];
        var handle_name = mode_order[i][1];
        var storage = mode_order[i][2];
        for (var key in changes) {
            var change = changes[key];
            if (key in storage) {
                storage[key].change(change);
            } else if (test(changes[key])) {
                this[handle_name](key, change);
            }
        }
    }

    // Increment the current round
    if (incround) {
        this.curround++
    }

    // Handle basic updates
    for (var uid in this.meshes) {
        if (!this.meshes[uid].update(this.curround, timefrac)) {
            delete this.meshes[uid]
        }
    }

    // Draw the scoreboard if necessary
    //if (this.gameover && !('scoreboard' in this.sprites)) {
        //var show_scoreboard = true
        //for (var id in this.sprites) {
            //if (this.sprites[id].intent === 'die') {
                //show_scoreboard = false
                //break
            //}
        //}
        //if (show_scoreboard) {
            //this.addSprite('scoreboard', new replay.Scoreboard({
                //viewport: this.viewport,
                //winner_str: this.winner_str
            //}))
        //}
    //}

    // Update the camera
	this.camera.lookAt(this.scene.position)
    this.renderer.render(this.scene, this.camera)
}
