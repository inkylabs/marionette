/*
 *   Copyright 2011 Inkylabs et al.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License")
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * <Insert Marionetastical limerick here>
 *
 */
replay.Mesh = function(options) {
    //THREE.Mesh.call(this, replay.resources[options.typename],
    var resource_name = 'cerberus'
    if (options.type === 'grutonium') {
        resource_name = 'grutonium'
    }
    THREE.Mesh.call(this, replay.resources[resource_name],
                    new THREE.MeshFaceMaterial())
    this.scale.set(10, 10, 10)
    this.incremental_vars = [
        'pos',
    ]
    this.update_methods = [
        'update_pos',
    ]
    //this.intVars = ['x', 'y'].concat(options.intVars || [])
    //var change = $.extend({}, options)
    //this.anims = {}
    //var that = this
    //for (key in change.anims) {
        //this.anims[key] = change.anims[key].slice()
        //if (key === 'die') {
            //this.anims[key].on_end = function() {
                //that.end = true
            //}
        //}
    //}
    //delete change.intVars
    //delete change.anims
    this.change(options)
    //this.prevIntent = null
}
replay.Mesh.prototype = new THREE.Mesh


replay.Mesh.prototype.interpolate = function(prev, next, frac) {
    return prev + frac * (next - prev);
}


replay.Mesh.prototype.interpolate_int = function(prev, next, frac) {
    return parseInt(this.interpolate(prev, next, frac));
}


replay.Mesh.prototype.update_pos = function(timefrac) {
    this.position.x = this.interpolate_int(this.pos_prev[0],
                                           this.pos_next[0], timefrac)
    this.position.y = this.interpolate_int(this.pos_prev[1],
                                           this.pos_next[1], timefrac)
    return true
}

//replay.Sprite.prototype.updateAnim = function(timefrac) {
    //var curAnim = this.anims[this.intent] || this.anims['default']
    //if (curAnim !== this.anim) {
        //this.anim = curAnim
        //this.anim.index = 0
    //}
    //this.setImage(this.anim.next())
//}


replay.Mesh.prototype.update = function(round, timefrac) {
    // TODO: Make this its own animation thing
    var len = this.morphTargetInfluences.length
    for (var i = 0; i < this.morphTargetInfluences.length; i++) {
        this.morphTargetInfluences[i] = 0
    }
    var pos = (new Date().getTime() / 50) % len
    var keyframe = parseInt(pos)
    var frac = pos % 1;  // + animOffset
    this.morphTargetInfluences[keyframe] = 1 - frac
    this.morphTargetInfluences[(keyframe + 1) % len] = frac

    // The round passed to us is the round that we don't yet have info about
    round--
    // Update all incremental variables needing to be updated
    if (round > this.round) {
        this.round = round
        for (var i = 0; i < this.incremental_vars.length; i++) {
            var name = this.incremental_vars[i]
            this.incremental_change(name, this[name + '_next'])
        }
    }

    // Call all update methods
    var ret = true;
    for (var i = 0; i < this.update_methods.length; i++) {
        ret = ret && this[this.update_methods[i]](timefrac);
    }
    return ret;
}


replay.Mesh.prototype.incremental_change = function(name, val) {
    var prev = name + '_prev'
    var next = name + '_next'
    if (this[next] === undefined) {
        this[prev] = val
    } else {
        this[prev] = this[next]
    }
    this[next] = val
}


//replay.Sprite.prototype.changeEnd = function(end) {
    //if (this.anims.die) {
        //return
    //}
    //this.end = end
//}


//replay.Sprite.prototype.change_pos = function(pos) {
    //this.do_default_change('position.x', pos[0])
    //this.do_default_change('position.y', pos[1])
//}

replay.Mesh.prototype.change = function(change) {
    //this.defaultChangesMade = []

    for (var name in change) {
        //var fname = 'change_' + name
        //if (typeof(this[fname]) === 'function') {
            //this[fname](change[name])
        //} else if (this.intVars.indexOf(name) === -1) {
            //this[name] = change[name]
        if (this.incremental_vars.indexOf(name) >= 0) {
            this.incremental_change(name, change[name])
        } else {
            this[name] = change[name]
        }
    }

    //// Make changes for all intVars
    //for (var i = 0; i < this.intVars.length; i++) {
        //var name = this.intVars[i]
        //if (this.defaultChangesMade.indexOf(name) === -1) {
            //this.doDefaultChange(name, this[name + 'Next'])
        //}
    //}
}

//replay.Sprite.prototype.draw = function() {
    //for (var name in this) {
        //if (name.indexOf('draw') === 0 &&
            //name !== 'draw' &&
            //typeof(this[name]) === 'function') {
            //this[name]()
        //}
    //}
    //jaws.Sprite.prototype.draw.call(this)
//}
