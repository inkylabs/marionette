#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from datetime import timedelta
from djangoappengine.settings_base import *
from os import getenv
from os.path import dirname, join as path_join, realpath
from random import choice
from sys import argv
from string import digits, letters, punctuation


TEST = '--settings' not in argv and len(argv) > 1 and argv[1] == 'test'
TEST_WITH_CODE_DIR = False
DEPLOYED = len(argv) == 1 and not argv[0]
DEBUG = not (TEST or DEPLOYED)
TEMPLATE_DEBUG = DEBUG

if not DEPLOYED:
    from platform import node
    import os
    os.environ['SERVER_NAME'] = node()
    os.environ['SERVER_PORT'] = '8000'
    if len(argv) > 2 and argv[1] == 'runserver':
        if ':'  in argv[2]:
            os.environ['SERVER_PORT'] = argv[2].split(':')[1]
        else:
            os.environ['SERVER_PORT'] = argv[2]
    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

ADMINS = (
    ('Conley Owens', 'cco3@inkylabs.com'),
)
MANAGERS = ADMINS

#DATABASES['native'] = DATABASES['default']
AUTOLOAD_SITECONF = 'mwautoload'

SECRET_KEY = ''.join([choice(letters + digits + punctuation)
                      for i in range(50)])

INSTALLED_APPS = (
    # third-party
    'annoying',
    'autoload',
    'compress',
    'djangoappengine',
    'djangotoolbox',
    'filetransfers',

    # marionette
    'docs',
    'gamequeue',
    'minicron',
    'mwbase',
    'profiles',
    'replays',
)

TIME_ZONE = 'UTC'
LANGUAGE_CODE = 'en-us'
SITE_NAME = 'MutantMarionette'
SITE_DOMAIN = 'mutantmarionette.com'
USE_I18N = True
USE_L10N = False
DATETIME_FORMAT = 'Y-m-d H:i'

PROJECT_ROOT = realpath(dirname(__file__))
MEDIA_ROOT = path_join(PROJECT_ROOT, 'media')
MEDIA_URL = '/media/'
# NOTE: We need to keep this so that the admin_media doesn't override our
#       normal media.
ADMIN_MEDIA_PREFIX = '/admin_media/'

TEMPLATE_LOADERS = [
    'django.template.loaders.filesystem.Loader',
]
if DEBUG:
    # Needed for compress
    TEMPLATE_LOADERS.append('django.template.loaders.app_directories.Loader')
TEMPLATE_DIRS = (
    path_join(PROJECT_ROOT, 'templates'),
)
TEMPLATE_CONTEXT_PROCESSORS = (
    'mwbase.context_processors.settings',
)

MIDDLEWARE_CLASSES = (
    # This loads the index definitions, so it has to come first
    'autoload.middleware.AutoloadMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.cache.UpdateCacheMiddleware',
    'django.middleware.cache.FetchFromCacheMiddleware',
)

ROOT_URLCONF = 'urls'

COMPRESS = not DEBUG
COMPRESS_AUTO = False
COMPRESS_CSS = {
    'main': {
        'source_filenames': (
            # third party
            'css/jquery.snippet.css',

            # marionette
            'css/base.css',
            'css/docs.css',
            'css/profiles.css',
            'css/replays.css',
        ),
        'output_filename': 'css/main.css',
    },
}
COMPRESS_JS = {
    'main': {
        'source_filenames': (
            # third party
            'js/jquery.js',
            'js/jquery.dotimeout.js',
            'js/jquery.form.js',
            'js/jquery.snippet.js',
            'js/jquery.ui.js',
            'js/facybox.js',
            'js/jaws.js',

            # marionette
            'js/replay/base.js',
            'js/replay/sprite.js',
            'js/replay/world.js',
            'js/replay/dashboard.js',
            'js/replay/scoreboard.js',
            'js/replay/item.js',
            'js/replay/unit.js',
            'js/replay/player.js',
            'js/replay/game.js',
            'js/replay/replay.js',

            # marionetteweb
            'js/base.js',
            'js/docs.js',
            'js/profiles.js',
            'js/replays.js',
        ),
        'output_filename': 'js/main.js',
    },
}

# TODO: Reenable our custom logging
#       There's a pretty weird error where
#       python2.7/logging/__init__.py Logger:remove_handler sucks up of
#       memory until everything crashes.  It's really weird because it
#       does this on a list.remove which is pretty standard.
#LOGGING = {
    #'version': 1,
    #'disable_existing_loggers': True,
    #'formatters': {
        #'default': {
            #'format': '%(asctime)s %(levelname)s %(name)s %(message)s',
        #},
    #},
    #'handlers': {
        #'default':{
            #'level':'DEBUG',
            #'class':'logging.StreamHandler',
            #'formatter': 'default'
        #},
    #},
    #'loggers': {
        #'': {
            #'handlers':['default'],
            #'propagate': True,
            #'level':'DEBUG',
        #},
    #}
#}

CACHE_MIDDLEWARE_SECONDS = 5 * 60 if not DEBUG else 0

VERSION = getenv('MWVERSION', '0.2')
MIN_USER_VERSION = "0.2"
MIN_WORKER_VERSION = "0.2"
GAMEQUEUE_FLAGGING_PENALTY_HOURS = 1
GAMEQUEUE_NUM_PLAYERS_TO_FETCH = 100
GAMEQUEUE_SCHEDULE_PERIOD_DAYS = 7

REPLAYS_PAGE_SIZE=10
REPLAYS_MAX_STORED=1000

TEST_RUNNER = 'mwbase.test.SmartTestSuiteRunner'
NOSE_ARGS = ['--logging-clear-handlers', '--logging-filter=-root']

PRE_DEPLOY_COMMANDS = ['synccompress']

SEARCH_BACKEND = 'search.backends.immediate_update'
