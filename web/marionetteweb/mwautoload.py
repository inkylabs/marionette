#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from minicron import autodiscover as minicron_autodiscover
import search

from logging import getLogger


logger = getLogger(__name__)
logger.info('autoloading')

minicron_autodiscover()

# Future: django > 1.3 - Remove this when they don't try to force csrf on us
import django.template.context
django.template.context._builtin_context_processors =  ()

# Docs.info reads a lot of files, so we want to do it early
import docs.info

# search for "search_indexes.py" in all installed apps
search.autodiscover()
