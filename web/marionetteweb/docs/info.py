#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from mwbase.utils import version_at_least


from django.conf import settings
from django.utils.translation import ugettext as _
from json import load as json_load
from os import listdir
from os.path import dirname, join as path_join, relpath


# Units

unit_base = None
unit_defs = {}
defs_path = path_join(dirname(__file__), 'unit_defs')
for filename in listdir(defs_path):
    if filename.startswith('.'):
        continue
    data = json_load(open(path_join(defs_path, filename)))
    if filename == 'base.json' or not filename.endswith('.json'):
        unit_base = data
    else:
        unit_defs[data['typename']] = data

unit_descriptions = {
    'toad': _(u''
        'These anurac amphibians rumble in the dead of night, menacing and '
        'clandestine.  They are feared not for their size, but for their '
        'determination and their carnivorous blood-lust.'),
    'brawlmander': _(u''
        'This unit has front claws sharpened to a razor edge, and a thick, '
        'muscular tail.  The very tongue of this urodelic mercenary will '
        'tear any army to shreads.'),
}

# The key is the attribute name, followed by a string describing the method.
unit_values = {
    'attack_time_req': _(u''
        'The amount of time it takes to make an attack.'),
    'effects': _(u''
        'A dictionary containing different effects on a unit.'),
    'health': _(u''
        'A float representing current health.'),
    'inventory': _(u''
        'A dictionary mapping an item type (e.g. Grutonium) to the '
        'number of that item in posession.'),
    'inventory_weight': _(u''
        "The total weight of everything in a unit's inventory."),
    'items_in_view': _(u''
        'A list of items within range_of_sight.'),
    'max_health': _(u''
        'A float representing max health.'),
    'max_inventory_weight': _(u''
        'A amount a weight a unit can store in their inventory.'),
    'max_memory': _(u''
        'The maximum amount of memory a unit may use.'),
    'max_time': _(u''
        'The maximum amount of wall time (in seconds) that a unit may use '
        'act.'),
    'melee_strength': _(u''
        'Strength when using a melee attack'),
    'metabolism': _(u''
        'A dictionary representing the effects of consumed items.'),
    'min_user_level': _(u''
        'The level a user has to be to have access to this unit.'),
    'obstacles_in_view': _(u''
        'A list of obstacle objects within range_of_sight. '),
    'pos': _(u''
        'A tuple (x, y) representing position in the world.  Position is '
        "calculated by adding a random offset to the unit's absolute position, "
        'so a unit cannot know where in the world it is.'),
    'range_of_sight': _(u''
        'An integer representing the maximum distance an  object can be, and '
        'still be seen.'),
    'rounds_committed': _(u''
        'Current number of rounds that the unit is committed to a given '
        'action and cannot start a new one.'),
    'running_speed': _(u''
        'How fast the unit moves.'),
    'size': _(u''
        'How much space this unit takes up.'),
    'spawn_items_req': _(u''
        'A dictionary denoting how long it takes to spawn a given unit type.'),
    'spawn_time_req': _(u''
        'A dictionary denoting what resources are reqired to spawn a given '
        'unit type.'),
    'typename': _(u''
        'A string with the name of this unit type.'),
    'units_in_view': _(u''
        'A list of units within range_of_sight.'),
}
if version_at_least('0.2.1'):
    unit_values['pos'] = _(u''
        'A tuple (x, y) representing position of the unit relative to where '
        'it was created.  Each unit begins with pos = (0, 0).')

# Attributes we want to show up on the unit's doc page
unit_attr_whitelist = [
    'attack_time_req',
    'effects',
    'max_health',
    'max_inventory_weight',
    'max_time',
    'max_memory',
    'melee_strength',
    'metabolism',
    'min_user_level',
    'range_of_sight',
    'running_speed',
    'size',
    'spawn_items_req',
    'spawn_time_req',
    'typename',
]

# The key is the method name, and the value is a tuple, ([list of arguments],
# 'string describing method').
unit_actions = {
    'attack': ('uid', _(u''
        'Attack another unit')),
    'die': ('', _(u''
        'Unit commits suicide.  Its items are dropped in the surrounding '
        'area.  NOTE: this action is broken and will be fixed in version'
        '0.2.1.')),
    'drop': ('typename, amount', _(u''
        'Unit drops amount of typename in its current position.  If the '
        'unit does not have enough of the typename, it drops as many as it '
        'has.')),
    'eat': ('typename, amount', _(u''
        'If a unit has this type of item in its inventory, and this item is '
        "edible, then it's removed from the inventory, and the unit's health "
        'increases.  The amount of health increase varies depending on the '
        'item and on the metabolism of the unit.')),
    'grab': ('uid, amount', _(u''
        'When an item is on the ground, it has an id.  A pile of multiple '
        'items (five grutonium, for example), would have only one id.  '
        '"Amount" is how many of the item the unit will take, if there are '
        'multiple.  If the unit tries to pick up more items than are there, '
        'it takes them all.  (Note: the amount a unit can carry is '
        'limited by its max_inventory_weight.)')),
    'idle': ('', _(u''
        'Unit waits a round. NOTE: this action is broken, and will be'
        'fixed in version 0.2.1.')),
    'move': ('x, y', _(u''
        'Unit moves towards the position (x, y) as far as it can, given its '
        'speed. A given (x, y) does not mean the same thing for each unit '
        '(see unit.pos for more details).  If there is an obstacle in the '
        'path, the unit will get as close as it can.')),
    'spawn': ('typename', _(u''
        'Unit creates another unit of type typename, and loses the items '
        'specified by its spawn_items_req variable.')),
}


# Items

item_base = None
item_defs = {}
defs_path = path_join(dirname(__file__), 'item_defs')
for filename in listdir(defs_path):
    if filename.startswith('.'):
        continue
    data = json_load(open(path_join(defs_path, filename)))
    if filename == 'base.json' or not filename.endswith('.json'):
        item_base = data
    else:
        item_defs[data['typename']] = data

item_descriptions = {
    'grutonium': _(u''
        'Glowing with the energy a thousand atom bombs, these fleshy, '
        'spore-bearing fungi can be found littering every square foot of '
        'dirt. They are known for the amphetamine rush they give when eaten, '
        'as well as the accute physical addiction that accompanies it.'),
}

item_values = {
    'pos': _(u''
        'A tuple (x, y) representing position in the world.'),
    'size': _(u''
        'A tuple (x, y) representing the units size.'),
    'typename': _(u''
        'A string with the name of this item type.'),
    'weight_per_unit': _(u''
        'Weight per unit of an item.'),
}


# colorlist

_defs_dir = path_join(dirname(__file__), 'colorlist')

def _get_names(listname):
    for line in open(path_join(_defs_dir, listname + '.txt')):
        line = line.strip()
        if line and line[0] != '#':
            yield line

blacklist = list(_get_names('kw_blacklist'))
whitelist = list(_get_names('mod_whitelist'))
