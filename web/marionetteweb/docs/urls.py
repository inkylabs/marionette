#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from django.conf.urls.defaults import patterns, url
from django.views.generic.simple import direct_to_template


urlpatterns = patterns('docs.views',
    # general pages
    url(r'^$', direct_to_template, {'template': 'docs/root.html'},
        name='docs.root'),
    url(r'^intro/$', direct_to_template, {'template': 'docs/intro.html'},
        name='docs.intro'),
    url(r'^requirements/$', direct_to_template,
        {'template': 'docs/requirements.html'}, name='docs.requirements'),
    url(r'^settings/$', direct_to_template,
        {'template': 'docs/settings.html'}, name='docs.settings'),
    url(r'^marionette_commands/$', direct_to_template, {'template':
        'docs/commands.html'}, name='docs.commands'),
    url(r'^sandboxing/$', 'sandboxing', name='docs.sandboxing'),
    # units
    url(r'^units/$', 'units', name='docs.units'),
    url(r'^units/(?P<typename>\w+)$', 'unit', name='docs.unit'),
    # items
    url(r'^items/$', 'items', name='docs.items'),
    url(r'^items/(?P<typename>\w+)$', 'item', name='docs.item'),
    # modules
    url(r'^marionette.game/$', direct_to_template, {'template': 'docs/'
        'marionette.game.html'}, name='docs.marionette.game'),
    url(r'^marionette.items/$', direct_to_template, {'template': 'docs/'
        'marionette.items.html'}, name='docs.marionette.items'),
    url(r'^marionette.logging/$', direct_to_template, {'template': 'docs/'
        'marionette.logging.html'}, name='docs.marionette.logging'),
    url(r'^marionette.settings/$', direct_to_template, {'template': 'docs/'
        'marionette.settings.html'}, name='docs.marionette.settings'),
    url(r'^marionette.utils.spatial/$', direct_to_template, {'template': 'docs/'
        'marionette.utils.spatial.html'}, name='docs.marionette.utils.spatial'),
    url(r'^marionette.units/$', direct_to_template, {'template': 'docs/'
        'marionette.units.html'}, name='docs.marionette.units'),
)
