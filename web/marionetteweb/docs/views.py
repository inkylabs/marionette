#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from docs import info
from docs.utils import from_camel_case, to_camel_case
from profiles.models import Profile

from annoying.decorators import render_to
from django.http import Http404
from json import dumps as json_dumps


@render_to('docs/unit.html')
def unit(request, typename):
    try:
        unit_def = info.unit_defs[typename]
    except KeyError:
        raise Http404
    actions = []
    values = []
    unit_stats = {}
    for attribute in unit_def['attributes']:
        for key in info.unit_base['actions'].get(attribute, []):
            params = info.unit_actions[key][0]
            desc = info.unit_actions[key][1]
            actions.append((key, params, desc))
        for key, value in info.unit_base['values'].get(attribute, {}).items():
            desc = info.unit_values[key]
            values.append((key, desc))
            if key in info.unit_attr_whitelist:
                unit_stats[key] = json_dumps(value)
    for attribute in unit_def:
        if attribute in info.unit_values:
            values.append((attribute, info.unit_values[attribute]))
    actions.sort()
    values.sort()
    for upgrade, vals in unit_def['upgrades'].items():
        for key, value in vals[0].items():
            if key in info.unit_attr_whitelist:
                unit_stats[key] = json_dumps(value)
    return {
        'classname': to_camel_case(typename),
        'description': info.unit_descriptions[typename],
        'actions': actions,
        'typename': typename,
        'upgrades': unit_def['upgrades'],
        'unit_stats': unit_stats,
        'values': values,
    }


@render_to('docs/units.html')
def units(request):
    return {
        'units': [(to_camel_case(unit), unit) for unit in info.unit_defs]
    }


@render_to('docs/item.html')
def item(request, typename):
    try:
        item_def = info.item_defs[typename]
    except KeyError:
        raise Http404
    item_stats = {}
    values = []
    for attribute, value in item_def.items():
        item_stats[attribute] = json_dumps(value)
        values.append((attribute, info.item_values[attribute]))
    values.sort()
    return {
        'classname': to_camel_case(typename),
        'description': info.item_descriptions[typename],
        'item_stats': item_stats,
        'typename': typename,
        'values': values,
    }


@render_to('docs/items.html')
def items(request):
    return {
        'items': [(to_camel_case(item), item) for item in info.item_defs]
    }


@render_to('docs/sandboxing.html')
def sandboxing(request):
    return {
        'whitelist': info.whitelist,
        'blacklist': info.blacklist
    }
