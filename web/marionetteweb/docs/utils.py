#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
def from_camel_case(name):
    if not (type(name) == str or type(name) == unicode):
        name = name.__name__
    ret = name[:1].lower()
    for c in name[1:]:
        if c.isupper():
            ret += '_' + c.lower()
        else:
            ret += c
    return ret


def to_camel_case(name):
    ret = ''
    upper = True
    for c in name:
        if upper:
            ret += c.upper()
            upper = False
        elif c == '_':
            upper = True
        else:
            ret += c
    return ret
