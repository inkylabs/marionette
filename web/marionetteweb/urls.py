#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from django.conf.urls.defaults import include, patterns, url
from django.views.generic.simple import direct_to_template
from djangotoolbox.errorviews import server_error as handler500


urlpatterns = patterns('',
    # gae
    url(r'^_ah/warmup$', 'djangoappengine.views.warmup', name='warmup'),
    # base
    url(r'^$', 'mwbase.views.home', name='home'),
    url(r'^about$', direct_to_template, {'template': 'about.html'},
        name='about'),
    # documentation
    url(r'^docs/', include('docs.urls')),
    # gamequeue
    url(r'^', include('gamequeue.urls')),
    # minicron
    url(r'^_cron/$', 'minicron.views.cron', name='cron'),
    # profiles
    url(r'^users/', include('profiles.users_urls')),
    url(r'^', include('profiles.urls')),
    # replays
    url(r'^replays/', include('replays.replays_urls')),
    url(r'^', include('replays.urls')),
    # version
    url(r'^_version$', 'mwbase.views.version', name='version'),
)
