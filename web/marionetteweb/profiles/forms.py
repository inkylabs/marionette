#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# I don't mean to sound conceited,
# But most engineers have cheated.
#     Counting lines of code added
#     Leaves things mangy and matted.
# The best metric is lines deleted.
#
from .models import Profile
from mwbase.forms import ModelForm

from django.forms import ValidationError
from django.utils.translation import ugettext as _
from re import match as re_match


__all__ = (
    'RegisterForm',
)


class RegisterForm(ModelForm):
    class Meta:
        model = Profile
        fields = (
            'git_url',
        )

    def clean_git_url(self):
        git_url = self.cleaned_data['git_url']
        if git_url and not re_match(r'(https?|git)://', git_url):
            raise ValidationError(_(u'Git repository is not valid.'))
        profiles = [p.pk for p in Profile.objects.filter(git_url=git_url)]
        if len(profiles):
            msg = _(u'Git repository in use by user %d!') % profiles[0]
            raise ValidationError(msg)
        return git_url
