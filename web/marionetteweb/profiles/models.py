#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# In Santa Cruz there is a spot;
# That can tie a brain in a knot.
#     Is gravity faltering?
#     The air, mind-altering?
# Is the ground made of alien snot?
#
from mwbase.models import JSONField, Model, QuerySet
from django.conf import settings

from datetime import datetime
from django.conf import settings
from django.db.models import (BooleanField, CharField, DateTimeField,
                              PositiveIntegerField, permalink)
from django.template.defaultfilters import slugify
from django.utils.simplejson import dumps as json_dumps


__all__ = (
    'Profile',
)


default_img_url = settings.MEDIA_URL + 'img/defaultprofile.png'
_minxp = [0]
for i in range(1, 1000):
    _minxp.append(_minxp[-1] + i ** 2)


class Profile(Model):
    # full name of the user.
    name = CharField(max_length=100, blank=True, verbose_name='Full name',
                     default='Anonymous Swarmmaster')
    # upgrades this profile has
    upgrades = JSONField()
    # experience points for this profile
    xp = PositiveIntegerField(default=0)
    # the url of their git repository
    git_url = CharField(max_length=200, default='', blank=True)
    # the url of their profile image
    img_url = CharField(max_length=200, default=default_img_url)


    # == gamequeue fields ==
    # number of games the player has scheduled recently
    # as defined by settings.GAMEQUEUE_SCHEDULE_PERIOD
    num_games_scheduled_this_period = PositiveIntegerField(default=0)
    # whether or not the user's code ran correctly the last time we attempted
    flagged = BooleanField(default=False)
    # number of consecutive times we've flagged this users code
    num_consecutive_flaggings = PositiveIntegerField(default=0)
    # when we should unflag the user and try running their code again
    unflag_timestamp = DateTimeField(auto_now_add=True)

    class QuerySet(QuerySet):
        def clean_kwargs(self, kwargs):
            new_kwargs = {}
            for key, value in kwargs.items():
                if key.startswith('level'):
                    key = key.replace('level', 'xp')
                    value = _minxp[value]
                new_kwargs[key] = value
            return new_kwargs

        def create(self, *args, **kwargs):
            kwargs = self.clean_kwargs(kwargs)
            return super(QuerySet, self).create(*args, **kwargs)

        def filter(self, *args, **kwargs):
            kwargs = self.clean_kwargs(kwargs)
            return super(QuerySet, self).filter(*args, **kwargs)

    def __init__(self, *args, **kwargs):
        kwargs.setdefault('upgrades', {'Toad': {}})
        super(Profile, self).__init__(*args, **kwargs)
        self._level = 1

    def config(self):
        return {
            'uid': self.pk,
            'level': self.level,
            'git_url': self.git_url,
        }

    @property
    def level(self):
        # NOTE: we assume that self.xp can never decrease
        while self.xp >= _minxp[self._level]:
            self._level += 1
        return self._level

    @property
    def num_upgrades_available(self):
        num_upgrades_used = 0
        for unit_upgrades in self.upgrades.values():
            num_upgrades_used += sum(unit_upgrades.values())
        return self.level - num_upgrades_used - 1

    def calc_xp_current(self):
        return self.xp - _minxp[self.level - 1]

    def calc_xp_needed(self):
        return self.level ** 2

    def calc_xp_percent(self):
        return int(self.calc_xp_current() * 100.0 / self.calc_xp_needed())

    def calc_xp_generated(self):
        return self.level

    @permalink
    def get_absolute_url(self):
        return ('profiles.show', (), {
            'id': self.pk,
            'slug': slugify(self.name)
        })

    @permalink
    def get_replays_url(self):
        return ('replays.browse_user', (), {
            'uid': self.pk,
        })
