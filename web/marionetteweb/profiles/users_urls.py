#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from django.conf.urls.defaults import patterns, url
from django.views.generic.simple import direct_to_template


urlpatterns = patterns('profiles.views',
    # profile pages
    url(r'^$', 'browse', name='profiles.browse'),
    url(r'^(?P<id>\d+).json$', 'config', name='profiles.config'),
    url(r'^(?P<id>\d+)/(?P<slug>.*)$', 'show', name='profiles.show'),
    # search
    url(r'^search/$', direct_to_template, {'template': 'profiles/search.html'},
        name='profiles.search'),
    url(r'^_search/(?P<query>.+)$', 'fetch_users',
        name='profiles.fetch_users'),

)
