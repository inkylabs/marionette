#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from mwbase.test import UnitTest
from profiles.forms import *

from django.forms import ValidationError


class RegisterForm_clean_git_urlTest(UnitTest):
    def test_invalid_git_fails(self):
        form = RegisterForm()
        form.cleaned_data = {'git_url': 'invalidurl'}
        self.assertRaises(ValidationError, form.clean_git_url)

    def test_reuse_fails(self):
        self.create_profile(git_url='git://example.git')
        form = RegisterForm()
        form.cleaned_data = {'git_url': 'git://example.git'}
        self.assertRaises(ValidationError, form.clean_git_url)
