#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from profiles.models import *
from mwbase.test import UnitTest


class Profile_levelTest(UnitTest):
    def test_level(self):
        self.assertEquals(Profile(xp=0).level, 1)
        self.assertEquals(Profile(xp=1).level, 2)
        self.assertEquals(Profile(xp=2).level, 2)
        self.assertEquals(Profile(xp=4).level, 2)
        self.assertEquals(Profile(xp=5).level, 3)


class Profile_num_upgrades_availableTest(UnitTest):
    def go(self, upgrades):
        return Profile(xp=14, upgrades=upgrades).num_upgrades_available

    def test_num_upgrades_available(self):
        self.assertEquals(self.go({'A': {}}), 3)
        self.assertEquals(self.go({'A': {'a': 1}}), 2)
        self.assertEquals(self.go({'A': {'a': 2}}), 1)
        self.assertEquals(self.go({'A': {'a': 1}, 'B': {'a': 1}}), 1)


class Profile_calc_xp_currentTest(UnitTest):
    def test_current(self):
        self.assertEquals(Profile(xp=0).calc_xp_current(), 0)
        self.assertEquals(Profile(xp=1).calc_xp_current(), 0)
        self.assertEquals(Profile(xp=2).calc_xp_current(), 1)
        self.assertEquals(Profile(xp=4).calc_xp_current(), 3)
        self.assertEquals(Profile(xp=5).calc_xp_current(), 0)


class Profile_calc_xp_neededTest(UnitTest):
    def test_current(self):
        self.assertEquals(Profile(xp=0).calc_xp_needed(), 1)
        self.assertEquals(Profile(xp=1).calc_xp_needed(), 4)
        self.assertEquals(Profile(xp=2).calc_xp_needed(), 4)
        self.assertEquals(Profile(xp=4).calc_xp_needed(), 4)
        self.assertEquals(Profile(xp=5).calc_xp_needed(), 9)


class Profile_calc_xp_currentTest(UnitTest):
    def test_current(self):
        self.assertEquals(Profile(xp=0).calc_xp_percent(), 0)
        self.assertEquals(Profile(xp=1).calc_xp_percent(), 0)
        self.assertEquals(Profile(xp=2).calc_xp_percent(), 25)
        self.assertEquals(Profile(xp=4).calc_xp_percent(), 75)
        self.assertEquals(Profile(xp=5).calc_xp_percent(), 0)
