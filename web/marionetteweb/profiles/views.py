#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# A man once gave me a suggestion:
# Visit a library and question
#     If the pages are sour,
#     Then proceed to devour,
# Because books were made for digestion.
#
from .forms import RegisterForm
from .models import Profile
from mwbase.decorators import ajax_request

from annoying.decorators import render_to
from django.http import Http404
from django.shortcuts import get_object_or_404
from json import dumps as json_dumps
from logging import getLogger
from search.core import search as appsearch


__all__ = (
    'register',
    'register_git_url',
    'setmy',
    'show',
)
logger = getLogger(__name__)


@render_to('profiles/browse.html')
def browse(request):
    return {'profiles': Profile.objects.all()}

@render_to('profiles/show.html')
def show(request, id, slug):
    return {'profile': get_object_or_404(Profile, pk=id)}


@ajax_request(method='GET')
def config(request, id):
    return get_object_or_404(Profile, pk=id).config()


@ajax_request(method='POST')
def register_git_url(request):
    form = RegisterForm(request.POST)
    profile = form.ajax_save()
    return {'player_id': profile.pk}


@ajax_request(method='GET')
def fetch_users(request, query):
    ret = []
    for r in appsearch(Profile, query)[:20]:
        ret.append({
            'label': r.name,
            'img_url': r.img_url,
            'level': r.level,
            'url': r.get_absolute_url()
        })
    return {'results': ret}


@render_to('home.html')
def setmy(request):
    pk = request.GET.pop('id')
    Profile.objects.filter(pk=pk).update(**request.GET)
    return {}
