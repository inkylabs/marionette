#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from mwbase.test import UnitTest
from replays.cron import *
from replays.models import Replay, ReplayFile, ProfileReplay


class remove_stale_replaysTest(UnitTest):
    def test_removed(self):
        from django.conf import settings
        settings.REPLAYS_MAX_STORED = 1
        Replay.objects.all().delete()
        ProfileReplay.objects.all().delete()
        ReplayFile.objects.all().delete()
        profile = self.create_profile()
        replays = []
        for i in range(3):
            replay = Replay.objects.create()
            user_replay = ProfileReplay.objects.create(
                    replay=replay,
                    profile=profile,
                    created=replay.created,
                    last_viewed=replay.last_viewed)
            replay_file = ReplayFile.objects.create(
                    replay=replay,
                    pos=0,
                    last_viewed=replay.last_viewed)
            replays.append((replay, user_replay, replay_file))
        self.assertTrue(remove_stale_replays())
        for i in range(2):
            for obj in replays[i]:
                self.assertRaises(obj.__class__.DoesNotExist, obj.refresh)
        for obj in replays[2]:
            self.assertIsNotNone(obj.refresh())
