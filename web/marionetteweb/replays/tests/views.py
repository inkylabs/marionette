#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from mwbase.test import TestRequest, UnitTest
from replays.models import Replay, ReplayFile, ProfileReplay
from replays.views import *

from datetime import datetime, timedelta
from django.utils.simplejson import dumps as json_dumps, loads as json_loads


class create_replayTest(UnitTest):
    def test_created(self):
        profile = self.create_profile()
        response = create_replay(TestRequest(post={
            'users': json_dumps([profile.pk])
        }))
        data = json_loads(response.content)
        self.assertTrue(Replay.objects.filter(pk=data['id']).exists())
        self.assertTrue('upload_data' in data)
        self.assertGreater(len(data['upload_url']), 0)


class showTest(UnitTest):
    def test_last_viewed_updated(self):
        now = datetime.now() - timedelta(days=1)
        profile = self.create_profile()
        replay = Replay.objects.create(
                ready=True)
        replay.last_viewed = now
        replay.created=now
        replay.save()
        replay_user = ProfileReplay.objects.create(
                replay=replay,
                profile=profile,
                created=replay.created,
                last_viewed=replay.last_viewed)
        replay_file = ReplayFile.objects.create(
                replay=replay,
                pos=0,
                last_viewed=replay.last_viewed)
        before = datetime.now()
        response = show(TestRequest(), id=replay.pk)
        replay.refresh()
        self.assertLess(before, replay.last_viewed)
        replay_user.refresh()
        self.assertLess(before, replay_user.last_viewed)
        replay_file.refresh()
        self.assertLess(before, replay_file.last_viewed)
