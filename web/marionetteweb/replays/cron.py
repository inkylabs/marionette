#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from .models import Replay, ReplayFile, ProfileReplay
from minicron import register

from datetime import timedelta
from django.conf import settings


def remove_stale_replays():
    total = Replay.objects.all().count()
    num_to_remove = total - settings.REPLAYS_MAX_STORED
    if num_to_remove < 1:
        return
    to_remove = Replay.objects.all().order_by('last_viewed')[:num_to_remove]
    last = list(to_remove)[-1]
    Replay.objects.filter(last_viewed__lte=last.last_viewed).delete()
    ProfileReplay.objects.filter(last_viewed__lte=last.last_viewed).delete()
    ReplayFile.objects.filter(last_viewed__lte=last.last_viewed).delete()
    return True
register('replays.remove_stale_replays', remove_stale_replays,
         timedelta(minutes=30))
