#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from .forms import ReplayForm, ReplayFileForm
from .models import Replay, ReplayFile, ProfileReplay
from gamequeue.decorators import cluster_request
from mwbase.decorators import ajax_request
from mwbase.utils import getint, timestamp_or_404, tomorrow
from profiles.models import Profile

from annoying.decorators import render_to
from datetime import datetime, timedelta
from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from filetransfers.api import prepare_upload, serve_file
from logging import getLogger
from urllib import urlencode


logger = getLogger(__name__)


@cluster_request()
def replay_upload_params(request):
    view_url = reverse('replays.add_replay_file')
    upload_url, upload_data = prepare_upload(request, view_url)
    return {
        'upload_url': upload_url,
        'upload_data': upload_data,
    }


@cluster_request(method='POST')
def create_replay(request):
    form = ReplayForm(request.POST)
    replay = form.ajax_save()
    view_url = reverse('replays.add_replay_file')
    query_params = {}
    query_params['key'] = request.GET.get('key', '')
    query_params['version'] = request.GET.get('version', '')
    url = view_url + '?' + urlencode(query_params)
    upload_url, upload_data = prepare_upload(request, url)
    return {
        'id': replay.pk,
        'upload_url': upload_url,
        'upload_data': upload_data,
    }


@cluster_request()
def add_replay_file(request):
    view_url = reverse('replays.add_replay_file')
    query_params = {}
    query_params['key'] = request.GET.get('key', '')
    query_params['version'] = request.GET.get('version', '')
    if request.method == 'POST':
        form = ReplayFileForm(request.POST, request.FILES)
        if form.is_valid():
            replay_file = form.save()
            query_params['final'] = 'false'
            if form.cleaned_data['final']:
                query_params['final'] = 'true'
        else:
            error = form.errors.as_text()
            logger.error(error)
            query_params['error'] = error
        return HttpResponseRedirect(view_url + '?' + urlencode(query_params))
    if request.GET.get('final', 'false') == 'true':
        return {}
    url = view_url + '?' + urlencode(query_params)
    upload_url, upload_data = prepare_upload(request, url)
    return {
        'upload_url': upload_url,
        'upload_data': upload_data,
    }


def replay_file(request, replay, pos):
    replay_file = get_object_or_404(ReplayFile, replay=replay, pos=pos)
    return serve_file(request, replay_file.file)


@render_to('replays/browse.html')
def browse(request, start=None):
    start = timestamp_or_404(start, tomorrow())
    page_size = settings.REPLAYS_PAGE_SIZE
    replays = list(Replay.objects
                   .filter(ready=True, created__lte=start)
                   .order_by('-created')[:page_size + 1])
    next_replay = None
    if len(replays) > page_size:
        next_replay = replays[-1]
        replays = replays[:page_size]
    return {
        'next_replay': next_replay,
        'replays': replays,
    }


@render_to('replays/browse.html')
def browse_user(request, uid, start=None):
    start = timestamp_or_404(start, tomorrow())
    page_size = settings.REPLAYS_PAGE_SIZE
    profile = get_object_or_404(Profile, pk=uid)
    replays = list(ProfileReplay.objects
                   .filter(profile=profile, created__lte=start)
                   .order_by('-created')[:page_size + 1])
    next_replay = None
    if len(replays) > page_size:
        next_replay = replays[-1]
        replays = replays[:page_size]
    return {
        'next_replay': next_replay,
        'profile': profile,
        'replays': replays,
    }


@render_to('replays/show.html')
def show(request, id):
    replay = get_object_or_404(Replay, pk=id, ready=True)
    now = datetime.now()
    # Only update last_viewed if it has been a while since the replay was
    # last viewed
    if now - replay.last_viewed > timedelta(days=1):
        # Order is important here because if a delete happens in the middle of
        # this we want the Replay and ProfileReplays to get deleted before the
        # ReplayFiles
        Replay.objects.filter(pk=id).update(last_viewed=now)
        ProfileReplay.objects.filter(replay=id).update(last_viewed=now)
        ReplayFile.objects.filter(replay=id).update(last_viewed=now)
    return {
        'replay': replay
    }
