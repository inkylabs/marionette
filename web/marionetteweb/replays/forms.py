#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from .models import Replay, ReplayFile, ProfileReplay
from mwbase.forms import ModelForm
from profiles.models import Profile

from logging import getLogger
from django.forms import BooleanField


logger = getLogger(__name__)


class ReplayForm(ModelForm):
    class Meta:
        model = Replay
        fields = (
            'users',
        )


class ReplayFileForm(ModelForm):

    final = BooleanField(required=False)

    class Meta:
        model = ReplayFile
        fields = (
            'replay',
            'pos',
            'file',
        )

    def save(self, commit=True):
        replay = self.cleaned_data['replay']
        self.instance.last_viewed = replay.last_viewed
        ret = super(ReplayFileForm, self).save(commit=commit)
        if self.cleaned_data['final']:
            Replay.objects.filter(pk=replay.pk).update(ready=True)
            for user in replay.users:
                profile = Profile(pk=user)
                ProfileReplay.objects.create(replay=replay, profile=profile,
                                             created=replay.created,
                                             last_viewed=replay.last_viewed)
        return ret
