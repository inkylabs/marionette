#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from mwbase.models import JSONField, Model, permalink
from profiles.models import Profile

from django.db.models import (BooleanField, DateTimeField, FileField,
                              ForeignKey, PositiveIntegerField)


__all__ = (
    'ProfileReplay',
    'Replay',
    'ReplayFile',
)


class Replay(Model):
    ready = BooleanField(default=False)
    # Timestamp when this was created
    created = DateTimeField(auto_now_add=True)
    # Timestamp when this was last viewed
    last_viewed = DateTimeField(auto_now_add=True)
    # list of user ids
    users = JSONField()

    @permalink
    def get_absolute_url(self):
        return ('replays.show', (), {
            'id': self.pk,
        })

    @permalink
    def get_next_url(self):
        return ('replays.browse', (), {
            'start': self.created.isoformat(),
        })


class ReplayFile(Model):
    # the actual replay
    replay = ForeignKey(Replay, related_name='files')
    # The position of the file, [0, inf)
    pos = PositiveIntegerField()
    # The file containing a portion of the replay
    file = FileField(upload_to='replays/')
    # last_viewed = replay.last_viewed
    last_viewed = DateTimeField()


class ProfileReplay(Model):
    # the actual replay
    replay = ForeignKey(Replay, related_name='user_replays')
    # user associated with the replay
    profile = ForeignKey(Profile, related_name='user_replays')
    # created = replay.timestamp
    created = DateTimeField()
    # last_viewed = replay.last_viewed
    last_viewed = DateTimeField()

    @permalink
    def get_absolute_url(self):
        return ('replays.show', (), {
            'id': self.replay.pk,
        })

    @permalink
    def get_next_url(self):
        return ('replays.browse_user', (), {
            'user': self.profile.pk,
            'start': self.created.isoformat(),
        })
