/*
 *   Copyright 2011 Inkylabs et al.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * <Insert Marionetastical limerick here>
 *
 */
$.fn.hooklessCSS = function(name) {
    var hooks = $.cssHooks;
    $.cssHooks = {};
    var ret = $(this).css(name);
    $.cssHooks = hooks;
    return ret;
}


$.fn.mwAjaxForm = function(callback) {
    $(this).ajaxForm({
        dataType: 'json',
        error: function(xhr, e, status) {
            if (xhr.status !== 200) {
                $.facybox('<div class="mwAjax-error">' +
                          'Server returned error code ' + xhr.status + '.  ' +
                          status + '</div>');
            }
        },
        success: function(data, statusText, xhr, form) {
            if (!('errors' in data)) {
                form.each(callback, [data]);
            }
            else {
                var errorstr = '';
                for (var i = 0; i < data.errors.length; i++) {
                    errorstr += '<p>' + data.errors[i] + '</p>';
                }
                $.facybox('<div class="mwAjax-error">' + errorstr + '<div>');
            }
        }
    });
};


$.fn.mwFadeOut = function(callback) {
    return $(this).animate({opacity: '0'}, 500, function () {
        $(this).css('overflow', 'hidden');
        $(this).animate({
            width: '0px',
            height: '0px',
            'border-width': '0px',
            'margin-top': '0px',
            'margin-bottom': '0px',
            'margin-left': '0px',
            'margin-right': '0px',
            'padding-top': '0px',
            'padding-bottom': '0px',
            'padding-left': '0px',
            'padding-right': '0px',
        }, 500, callback);
    });
};


$.fn.fadeHide = function(callback) {
    $(this).each(function() {
        $.data($(this)[0], 'fadeHideOverflow', $(this).css('overflow'));
        var widthHook = $.cssHooks.width;
        var heightHook = $.cssHooks.height;
        delete $.cssHooks.width;
        delete $.cssHooks.height;
        var width = $(this).css('width');
        var height = $(this).css('height');
        $.data($(this)[0], 'fadeHideCss', {
            width: width,
            height: height,
            'border-width': $(this).css('border-width'),
            'margin-top': $(this).css('margin-top'),
            'margin-bottom': $(this).css('margin-bottom'),
            'margin-left': $(this).css('margin-left'),
            'margin-right': $(this).css('margin-right'),
            'padding-top': $(this).css('padding-top'),
            'padding-bottom': $(this).css('padding-bottom'),
            'padding-left': $(this).css('padding-left'),
            'padding-right': $(this).css('padding-right'),
        });
    });
    return $(this).mwFadeOut(function() {
        $(this).hide(callback);
    });
}


$.fn.unFadeHide = function(cssDelayed) {
    if (cssDelayed === undefined) cssDelayed = true;
    var cssDelay = cssDelayed ? 500 : 0;
    $(this).show();
    return $(this).each(function() {
        var css = $.data($(this)[0], 'fadeHideCss') || {};
        $(this).animate(css, cssDelay, function() {
            var overflow = $.data($(this)[0], 'fadeHideOverflow');
            if (overflow) {
                $(this).css('overflow', overflow);
            }
            $(this).animate({opacity: '1'}, 500);
        });
    });
}


$.fn.fadeDelete = function() {
    $(this).mwFadeOut(function() {
        $(this).remove();
    });
}


$.fn.mwFadeIn = function() {
    return $(this).animate({opacity: '1'}, 500);
};


window.console.log = function(text) {
    switch (text) {
        case '[jquery.form] terminating; zero elements found by selector':
          return;
        default:
    }
    console.log(text);
}
