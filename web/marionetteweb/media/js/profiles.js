/*
 *   Copyright 2011 Inkylabs et al.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * <Insert Marionetastical limerick here>
 *
 */
$(function() {
    // Register
    var register = $('#profiles-register');
    register.find('form').mwAjaxForm(
        function(data) {
            var player_id = data['player_id'];
            register.find('span').text(player_id);
            var a = register.find('.profile-link');
            a.attr('href', a.attr('href').replace('0', player_id));
            register.find('.turtle').fadeHide(function() {
                register.find('.jitb').unFadeHide();
            });
        }
    );
    // Replace source of broken img
    $('#profiles-img img').error(function() {
        $(this).attr('src', '/media/img/defaultprofile.png');
    });

    // User search
    $('#user_query').autocomplete({
            source: function(data, response) {
                $.getJSON('/users/_search/' + data['term'],
                    function(request) {
                        response(request['results'])
                    });
            },
            select: function(event, ui) {
                window.location = ui['item']['url'];
            }
    })
    // Overrides _renderItem to display more than user name
    .data('autocomplete')._renderItem = function(ul, item) {
        return $('<li id="profiles-search-li"></li>')
            .data('item.autocomplete', item)
            .append('<a href="' + item.url + '"><img src="' + item.img_url + '"></img>' + item.label + ' (lvl ' + item.level + ')</a>')
            .appendTo( ul );
    };
});
