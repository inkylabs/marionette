/*
 *   Copyright 2011 Inkylabs et al.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * <Insert Marionetastical limerick here>
 *
 */
$(function() {
    $('#docs-upgrade-table .choosable').click(
        function() {
            var upgrades = $(this).data()
            for (upgrade in upgrades) {
                var stat = $('#docs-' + upgrade + '-val');
                if (stat.length) {
                    var newVal = $(this).data()[upgrade];
                    stat.html(newVal);
                    $(this).parent('tr').children('td').each(function() {
                        if ($(this).data()[upgrade] <= newVal) {
                            $(this).removeClass('unchosen');
                            $(this).addClass('chosen');
                        }
                        else if (!$(this).hasClass('name')) {
                            $(this).removeClass('chosen');
                            $(this).addClass('unchosen');
                        }
                    });
                }
            }
        }
    );

    $('pre.bash').snippet('python', {style: 'darkness', showNum: false});
    $('pre.json').snippet('python', {style: 'darkness', showNum: false});
    $('pre.python').snippet('python', {style: 'darkness', showNum: false});
});

