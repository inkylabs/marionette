#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from .decorators import cluster_request
from .forms import DeleteUsersForm, FlagUsersForm, ReportResultsForm
from profiles.models import Profile

from django.conf import settings
from logging import getLogger


__all__ = (
    'delete_users',
    'fetch_players',
    'flag_users',
    'report_results',
)
logger = getLogger(__name__)


@cluster_request(method='POST')
def delete_users(request):
    form = DeleteUsersForm(request.POST)
    form.ajax_save()
    return {}


@cluster_request(method='GET')
def fetch_players(request):
    count = settings.GAMEQUEUE_NUM_PLAYERS_TO_FETCH
    profiles = Profile.objects.filter(flagged=False).order_by(
            'num_games_scheduled_this_period')[:count]
    return {
        'player_configs': [profile.config() for profile in profiles]
    }


@cluster_request(method='POST')
def flag_users(request):
    form = FlagUsersForm(request.POST)
    form.ajax_save()
    return {}


@cluster_request(method='POST')
def report_results(request):
    form = ReportResultsForm(request.POST)
    form.ajax_save()
    return {}
