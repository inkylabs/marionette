#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from django.conf.urls.defaults import include, patterns, url

urlpatterns = patterns('gamequeue.views',
    url(r'^delete_users$', 'delete_users', name='gamequeue.delete_users'),
    url(r'^fetch_players$', 'fetch_players', name='gamequeue.fetch_players'),
    url(r'^flag_users$', 'flag_users', name='gamequeue.flag_users'),
    url(r'^report_results$', 'report_results',
        name='gamequeue.report_results'),
)
