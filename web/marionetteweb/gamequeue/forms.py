#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from mwbase.forms import Form, JSONField
from profiles.models import Profile, default_img_url

from datetime import datetime, timedelta
from django.conf import settings
from django.db.models import F
from django.forms import ValidationError
from logging import getLogger


__all__ = (
    'DeleteUsersForm',
    'FlagUsersForm',
    'ReportResultsForm',
)
logger = getLogger(__name__)


class DeleteUsersForm(Form):
    player_ids = JSONField(value_type=int)

    def save(self, *args, **kwargs):
        Profile.objects.filter(pk__in=self.cleaned_data['player_ids']).delete()


class FlagUsersForm(Form):
    player_ids = JSONField(value_type=int)

    def save(self, *args, **kwargs):
        for player_id in self.cleaned_data['player_ids']:
            try:
                profile = Profile.objects.get(pk=player_id)
            except Profile.DoesNotExist:
                continue
            new_num = profile.num_consecutive_flaggings + 1
            penalty = timedelta(hours=settings.GAMEQUEUE_FLAGGING_PENALTY_HOURS)
            new_timestamp = datetime.now() + new_num * penalty
            Profile.objects.filter(pk=player_id).update(
                    flagged=True,
                    num_consecutive_flaggings=new_num,
                    unflag_timestamp=new_timestamp)


class ReportResultsForm(Form):
    results = JSONField(main_type=list, value_type=dict)

    def save(self):
        for result in self.cleaned_data['results']:
            kwargs = {
                'xp': F('xp') + result['xp_awarded'],
                'num_games_scheduled_this_period':
                        F('num_games_scheduled_this_period') +
                        result['games_played'],
                'num_consecutive_flaggings': 0,
                'upgrades': result['upgrades'],
                'name': result['name'],
                'img_url': result.get('img_url') or default_img_url,
            }
            git_url = result.get('git_url')
            if git_url:
                kwargs['git_url'] = git_url
            Profile.objects.filter(pk=result['player_id']).update(**kwargs)
