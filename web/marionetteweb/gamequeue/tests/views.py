#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from gamequeue.views import *
from mwbase.test import TestRequest, UnitTest
from profiles.models import Profile

from django.conf import settings
from django.utils.simplejson import loads as json_loads


class fetch_playersTest(UnitTest):
    def go(self, **kwargs):
        request = TestRequest(get=kwargs, cluster=True)
        response = fetch_players(request)
        return json_loads(response.content)

    def setUp(self):
        Profile.objects.all().update(flagged=True)

    def test_players_fetched(self):
        self.mock(settings, 'GAMEQUEUE_NUM_PLAYERS_TO_FETCH', 1)
        # This is the profile we expect to get back
        profile = self.create_profile(num_games_scheduled_this_period=2)
        # num_games_scheduled_this_period too high
        self.create_profile(num_games_scheduled_this_period=3)
        # not eligible
        self.create_profile(flagged=True)
        configs = self.go()['player_configs']
        self.assertEquals(len(configs), 1)
        self.assertEquals(configs[0]['uid'], profile.pk)
