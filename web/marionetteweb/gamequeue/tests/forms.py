#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# There once was a young datastore
# Who partied too hard, and what's more:
#     He filled up on data (see?)
#     With relational strategy
# And then he passed out on the floor.
#
from gamequeue.forms import *
from mwbase.test import UnitTest
from profiles.models import Profile

from datetime import datetime, timedelta
from django.conf import settings
from django.contrib.auth.models import User


class DeleteUsersForm__saveTest(UnitTest):
    def test_users_deleted(self):
        profile1 = self.create_profile()
        profile2 = self.create_profile()
        form = DeleteUsersForm()
        form.cleaned_data = {
            'player_ids': [profile1.pk, profile2.pk]
        }
        form.save()
        self.assertRaises(Profile.DoesNotExist, profile1.refresh)
        self.assertRaises(Profile.DoesNotExist, profile2.refresh)


class FlagUsersForm__saveTest(UnitTest):
    def test_user_flagged(self):
        self.mock(settings, 'GAMEQUEUE_FLAGGING_PENALTY_HOURS', 1)
        profile1 = self.create_profile()
        profile2 = self.create_profile(num_consecutive_flaggings=1)
        form = FlagUsersForm()
        form.cleaned_data = {
            'player_ids': [profile1.pk, profile2.pk]
        }
        form.save()
        profile1.refresh()
        self.assertTrue(profile1.flagged)
        self.assertEquals(profile1.num_consecutive_flaggings, 1)
        self.assertAlmostEquals(profile1.unflag_timestamp,
                                datetime.now() + timedelta(hours=1),
                                delta=timedelta(minutes=1))
        profile2.refresh()
        self.assertTrue(profile2.flagged)
        self.assertEquals(profile2.num_consecutive_flaggings, 2)
        self.assertAlmostEquals(profile2.unflag_timestamp,
                                datetime.now() + timedelta(hours=2),
                                delta=timedelta(minutes=1))


class ReportResultsForm__saveTest(UnitTest):
    def test_results_saved(self):
        # Create profiles
        self.profile1 = self.create_profile(
                xp=1, num_games_scheduled_this_period=1)
        self.profile2 = self.create_profile(
                xp=2, num_games_scheduled_this_period=2,
                num_consecutive_flaggings=10)

        # Create form
        form = ReportResultsForm()
        form.cleaned_data = {'results' : [{
            'player_id': self.profile1.pk,
            'xp_awarded': 1,
            'games_played': 3,
            'name': 'p1',
            'upgrades': {'speed': 1},
            'git_url': 'git://player1.git',
            'img_url': 'http://img1.com',
        }, {
            'player_id': self.profile2.pk,
            'xp_awarded': 2,
            'games_played': 4,
            'name': 'p2',
            'upgrades': {'defense': 1},
            'git_url': 'git://player2.git',
            'img_url': 'http://img2.com',
        }]}
        form.save()

        # Test results
        self.profile1.refresh()
        self.assertEquals(self.profile1.xp, 2)
        self.assertEquals(self.profile1.num_games_scheduled_this_period, 4)
        self.assertEquals(self.profile1.name, 'p1')
        self.assertEquals(self.profile1.upgrades, {'speed': 1})
        self.assertEquals(self.profile1.git_url, 'git://player1.git')
        self.profile2.refresh()
        self.assertEquals(self.profile2.xp, 4)
        self.assertEquals(self.profile2.num_games_scheduled_this_period, 6)
        self.assertEquals(self.profile2.num_consecutive_flaggings, 0)
        self.assertEquals(self.profile2.name, 'p2')
        self.assertEquals(self.profile2.upgrades, {'defense': 1})
        self.assertEquals(self.profile2.git_url, 'git://player2.git')
