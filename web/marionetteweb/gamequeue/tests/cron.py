#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from gamequeue.cron import *
from mwbase.test import UnitTest

from datetime import datetime, timedelta


class unflagTest(UnitTest):
    def test_unflagged(self):
        profile1 = self.create_profile(flagged=True)
        profile2 = self.create_profile(flagged=True,
                unflag_timestamp=datetime.now() + timedelta(hours=1))
        self.assertTrue(unflag())
        self.assertFalse(profile1.refresh().flagged)
        self.assertTrue(profile2.refresh().flagged)
