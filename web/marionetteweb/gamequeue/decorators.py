#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from .models import Cluster
from mwbase.decorators import ajax_request
from mwbase.errors import AjaxError

from django.conf import settings
from django.http import Http404
from logging import getLogger


__all__ = (
    'cluster_request',
)
logger = getLogger(__name__)


def cluster_request(method=None):
    def create_func(function):
        @ajax_request(method=method)
        def new_func(request, *args, **kwargs):
            if settings.DEPLOYED and not request.is_secure():
                logger.warning('Request is not secure.')
                raise AjaxError('Request is not secure.')
            if not Cluster.objects.filter(
                        key=request.GET.get('key', '')).exists():
                logger.warning('Invalid worker key.')
                raise AjaxError('Invalid worker key.')
            if request.GET.get('version', 0) < settings.MIN_WORKER_VERSION:
                err_str = ('Worker version too low: min version is %s'
                                % settings.MIN_WORKER_VERSION)
                logger.warning(err_str)
                raise AjaxError(err_str)
            return function(request, *args, **kwargs)
        return new_func
    return create_func
