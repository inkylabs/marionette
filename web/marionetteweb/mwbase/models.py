#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from annoying.fields import (AutoOneToOneField as BaseAutoOneToOneField,
                             JSONField as BaseJSONField)
from django.conf import settings
from django.core.serializers.json import DjangoJSONEncoder
from django.core.urlresolvers import reverse
from django.db.models import (CharField, F, Manager as BaseManager,
                              Model as BaseModel, PositiveIntegerField,
                              get_apps, get_models)
from django.db.models.query import QuerySet as BaseQuerySet
from django.db.models.signals import post_save
from django.utils.decorators import wraps
from django.utils.http import urlencode
from django.utils.simplejson import dumps as json_dumps


class AutoOneToOneField(BaseAutoOneToOneField):

    """AutoOneToOneField creates related object on first call if it doesn't
    exists yet.  In addition, on a save, it will create an uncreated
    instance.

    """

    def contribute_to_related_class(self, cls, related):
        super(AutoOneToOneField, self).contribute_to_related_class(cls,
                                                                   related)
        def add_self(sender, instance, created, **kwargs):
            if created:
                try:
                    related.model.objects.get(
                        **{self.related.field.name: instance})
                except related.model.DoesNotExist:
                    related.model.objects.create(
                        **{self.related.field.name: instance})
        post_save.connect(add_self, sender=cls, weak=False)


class JSONField(BaseJSONField):
    def get_db_prep_save(self, value, *args, **kwargs):
        value = json_dumps(value, cls=DjangoJSONEncoder)
        return super(JSONField, self).get_db_prep_save(value, *args, **kwargs)


class QuerySet(BaseQuerySet):
    def increment(self, key, amount=1):
        return self.update(**{key: F(key) + amount})

    def decrement(self, key, amount=1):
        return self.update(**{key: F(key) - amount})


class Manager(BaseManager):
     def get_query_set(self):
        if hasattr(self.model, 'QuerySet'):
            return self.model.QuerySet(self.model)
        return QuerySet(self.model)


Model = BaseModel
def _refresh(self):
    refreshed = self.__class__.objects.get(pk=self.pk)
    #from django.db.models.fields.related import RelatedManager
    for field in self.__class__._meta.get_all_field_names():
        if type(getattr(self, field)).__name__ == 'RelatedManager':
            continue
        setattr(self, field, getattr(refreshed, field))
    return self
Model.refresh = _refresh


def permalink(func):
    @wraps(func)
    def inner(*args, **kwargs):
        bits = func(*args, **kwargs)
        params = ''
        if len(bits) > 3:
            params = '?' + urlencode(bits[3])
        return reverse(bits[0], None, *bits[1:3]) + params
    return inner
