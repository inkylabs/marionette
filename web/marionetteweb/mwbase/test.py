#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
#import cssutils
from collections import defaultdict
from django.conf import settings
from django.contrib.sessions.backends.cache import SessionStore
from django.core import mail
from django.core.exceptions import ImproperlyConfigured
from django.core.management import call_command
from django.core.files.uploadedfile import UploadedFile
from django.db import connections, DatabaseError
from django.db.models import loading
from django.http import (HttpRequest, HttpResponse, HttpResponseRedirect,
        QueryDict)
from django.template.loaders.app_directories import Loader
from django.test import TestCase as DjangoTestCase
from django.utils.importlib import import_module
from django_nose import NoseTestSuiteRunner
#from html5lib import html5parser
#from jslint.spidermonkey import lint
from logging import getLogger
#from PIL import Image
from os.path import (dirname as path_dirname, exists as path_exists,
                     isdir as path_isdir, islink as path_islink,
                     join as path_join)
from shutil import rmtree
#from StringIO import StringIO
from sys import getdefaultencoding, getfilesystemencoding
from tempfile import mkdtemp


__all__ = (
    'UnitTest',
    'no_render',
)
logger = getLogger(__name__)


def app_setup(*args):
    """Sets up several apps.

    This is not a part of the TestCase class so that we can easily use this
    function for creating fixtures from the interactive shell.

    Parameters:
    *args - a list of app names

    """
    for app in args:
        settings.INSTALLED_APPS.append(app)
    loading.cache.loaded = False
    call_command('syncdb', interactive=False, verbosity=0)


# The following bit is taken from
# django.template.loaders.app_directories
# At compile time, cache the directories to search.
fs_encoding = getfilesystemencoding() or getdefaultencoding()
app_test_template_dirs = []
for app in settings.INSTALLED_APPS:
    try:
        mod = import_module(app)
    except ImportError, e:
        raise ImproperlyConfigured('ImportError %s: %s' % (app, e.args[0]))
    template_dir = path_join(path_dirname(mod.__file__), 'test_templates')
    if path_isdir(template_dir):
        app_test_template_dirs.append(template_dir.decode(fs_encoding))
# It won't change, so convert it to a tuple to save memory.
app_test_template_dirs = tuple(app_test_template_dirs)


class TemplateLoader(Loader):
    def get_template_sources(self, template_name, template_dirs=None):
        if not template_dirs:
            template_dirs = app_test_template_dirs
        for source in super(TemplateLoader, self).get_template_sources(
                template_name, template_dirs):
            yield source

# FUTURE: For html5lib versions higher than 0.90, the InBodyPhase and
# HTMLParser classes won't be needed.
#class InBodyPhase(html5parser.InBodyPhase):
    #def endTagForm(self, token):
        #print 'my method'
        #node = self.tree.formPointer
        #self.tree.formPointer = None
        #if node is None or not self.tree.elementInScope(node.name):
            #self.parser.parseError("unexpected-end-tag",
                                   #{"name":"form"})
        #else:
            #self.tree.generateImpliedEndTags()
            #if self.tree.openElements[-1].name != node.name:
                #self.parser.parseError("end-tag-too-early-ignored",
                                       #{"name": "form"})
                #self.tree.openElements.remove(node)
            #else:
                #self.tree.openElements.pop()


#class HTMLParser(html5lib.HTMLParser):
    #def __init__(self):
        #super(HTMLParser, self).__init__()
        #self.phases['inBody'] = InBodyPhase(self, self.tree)


class UnitTest(DjangoTestCase):
    apps = ()
    #_html_parser = HTMLParser()
    #_css_parser = cssutils.CSSParser()

    def mock(self, obj, attr, value):
        self._mocks.append((obj, attr, getattr(obj, attr)))
        setattr(obj, attr, value)

    def mockf(self, obj, attr, value=None):
        margs = []
        def f(*args, **kwargs):
            margs.append((args, kwargs))
            return value
        self.mock(obj, attr, f)
        return margs

    def create_profile(self, **kwargs):
        from profiles.models import Profile
        profile = Profile.objects.create(**kwargs)
        if 'unflag_timestamp' in kwargs:
            profile.unflag_timestamp = kwargs['unflag_timestamp']
            profile.save()
        return profile

    def all_temps(self, model):
        return model.objects.filter(pk__in=self.pks(model))

    def _pre_setup(self):
        """Override the _pre_setup to do the things we want."""
        if self.apps:
            self._app_setup()
        mail.outbox = []
        self._mocks = []

    def _post_teardown(self):
        """Override the _post_teardown to do the things we want."""
        for obj, attr, value in self._mocks:
            setattr(obj, attr, value)
        if self.apps:
            self._app_teardown()

    def _app_setup(self):
        """Add any test models to the db."""
        self._original_installed_apps = list(settings.INSTALLED_APPS)
        app_setup(*self.apps)

    def _app_teardown(self):
        """Restore the settings."""
        # NOTE: We don't have an analogous app_teardown function because there
        #       is not a need for one, and it would be inferior to the
        #       following, since it would do a syncdb
        settings.INSTALLED_APPS = self._original_installed_apps
        loading.cache.loaded = False

    def assertGreater(self, val1, val2, msg=None):
        """Python3k emulation."""
        self.assertTrue(val1 > val2, msg)

    def assertLess(self, val1, val2, msg=None):
        """Python3k emulation."""
        self.assertTrue(val1 < val2, msg)

    def assertGreaterEqual(self, val1, val2, msg=None):
        """Python3k emulation."""
        self.assertTrue(val1 >= val2, msg)

    def assertLessEqual(self, val1, val2, msg=None):
        """Python3k emulation."""
        self.assertTrue(val1 <= val2, msg)

    def assertRedirects(self, val1, val2, msg=None):
        if isinstance(val1, HttpResponse):
            self.assertTrue(isinstance(val1, HttpResponseRedirect),
                            msg=u'is type %s' % type(val1))
            self.assertTrue('location' in val1._headers)
            self.assertEqual(val1._headers['location'][1], val2)
        else:
            self.assertRedirects(val1, val2, msg=msg)

    def assertIn(self, member, container, msg=None):
        # For Python 2.7 or greater, use default assertIn
        """Just like self.assertTrue(a in b), but with a default message."""
        if member not in container:
            standardMsg = '%s not found in %s' % (member, container)
            self.fail(standardMsg)

    #def assertIsValidHtml5(self, response, msg=None):
        #document = self._parser.parse(response.content)
        #validHTML = True
        #if msg:
            #new_msg = msg
        #elif self._parser.errors:
            #ln = response.content.split('\n')
            #new_msg = 'Document is not valid HTML5'
            #for pos, errorcode, datavars in self._parser.errors:
                #start = 'Trailing solidus not allowed on element '
                #exceptions = ['link', 'img', 'input']
                #error = html5lib.constants.E.get(errorcode,
                        #'Unknown error "%s"' % errorcode) % datavars
                #ok = False
                #for e in exceptions:
                    #if error == start + e:
                        #ok = True
                #if not ok:
                    #validHTML = False
                    #new_msg += '\nLine %d Col %d ' % pos + error
                    #new_msg += '\n\t' + ln[pos[0] - 1][max(pos[1] - 30, 0):pos[1] + 30]
        #self.assertTrue(validHTML, new_msg)

    #def assertIsValidJS(self, filepath, msg=None):
        #f = map(str.strip, open(filepath).readlines())
        #errors = lint(filepath)
        #if msg:
            #new_msg = msg
        #elif errors:
            #new_msg = filepath + ' is not valid javascript.'
            #for error in errors:
                #line = error['line']
                #char = error['character']
                #new_msg += '\n' + ' '.join(['ln', str(line) + ',',
                                    #'col', str(char) + ',',
                                    #error['reason'],
                                    #f[line - 1][max(char - 30, 0):char + 30]
                #])
        #self.assertEqual(len(errors), 0, new_msg)

    #def assertIsValidCSS(self, filepath, msg=None):
        #mylog = StringIO()
        #h = logging.StreamHandler(mylog)
        #h.setFormatter(logging.Formatter('%(levelname)s %(message)s'))
        #cssutils.log.addHandler(h)
        #cssutils.log.setLevel(logging.INFO)
        #self._css_parser.parseFile(filepath)
        #if msg:
            #new_msg = msg
        #else:
            #new_msg = mylog.getvalue()
        #self.assertEqual(mylog.getvalue(), '', msg=new_msg)


cluster_key = ''


class SmartTestSuiteRunner(NoseTestSuiteRunner):
    """Persists the DB as do test_utils and djangosanetesting.
    Those two don't do quite what we want though.

    """

    def setup_databases(self, **kwargs):
        ret = super(SmartTestSuiteRunner, self).setup_databases(**kwargs)
        from gamequeue.models import Cluster
        global cluster_key
        cluster_key = Cluster.objects.create(name='test').key
        return ret

    def run_tests(self, test_labels, extra_tests=None):
        # We want to skip any symlink apps, so if the list is empty, we specify
        if not test_labels:
            test_labels = []
            for app in settings.INSTALLED_APPS:
                path = path_join(settings.PROJECT_ROOT, app)
                if path_exists(path) and not path_islink(path):
                    test_labels.append(app)
        super(SmartTestSuiteRunner, self).run_tests(test_labels, extra_tests)


class TestRequest(HttpRequest):

    def __init__(self, user=None, get={}, post=None, cluster=True):
        super(TestRequest, self).__init__()
        if user:
            self.user = user
        self.session = SessionStore()
        self._dont_enforce_csrf_checks = True
        if cluster:
            get.update({
                'key': cluster_key,
                'version': settings.MIN_WORKER_VERSION,
            })
        self.init_GET(get)
        if post != None:
            self.init_POST(post)

    def init_POST(self, data=None):
        self.method = 'POST'
        self.POST = QueryDict('', mutable=True)
        if data:
            self.POST.update(data)

    def init_GET(self, data=None):
        self.method = 'GET'
        self.GET = QueryDict('', mutable=True)
        if data:
            self.GET.update(data)


_unrendering = {}
import sys
import annoying.decorators
_render_to = annoying.decorators.render_to
_null_parameterized_decorator = lambda n: lambda f: f
for app in settings.INSTALLED_APPS:
    view_name = app + '.views'
    # Import it once to make sure its dependencies are
    # loaded normally
    try:
        __import__(view_name)
    except ImportError:
        continue
    # Backup the module
    backup = sys.modules[view_name]
    del sys.modules[view_name]
    ## Nullify the decorators
    annoying.decorators.render_to = _null_parameterized_decorator
    ## Reimport and save
    __import__(view_name)
    _unrendering[view_name] = sys.modules[view_name]
    ## Restore original values
    sys.modules[view_name] = backup
    annoying.decorators.render_to = _render_to


def no_render(func, *args, **kwargs):
    new_func = getattr(_unrendering[func.__module__], func.__name__)
    return new_func(*args, **kwargs)
