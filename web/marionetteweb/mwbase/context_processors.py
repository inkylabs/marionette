#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from django.conf import settings as site_settings


def settings(request):
    return {
        'DEBUG': site_settings.DEBUG,
        'DEPLOYED': site_settings.DEPLOYED,
        'LANGUAGE_CODE': site_settings.LANGUAGE_CODE,
        'MEDIA_URL': site_settings.MEDIA_URL,
        'SITE_NAME': site_settings.SITE_NAME,
    }
