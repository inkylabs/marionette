#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from .errors import AjaxError

from annoying.decorators import ajax_request as base_ajax_request
from logging import getLogger


logger = getLogger(__name__)


def ajax_request(method=None):
    def create_func(function):
        @base_ajax_request
        def new_func(request, *args, **kwargs):
            if method and request.method != method:
                return {'errors': ['Method must be %s.' % method]}
            try:
                ret = function(request, *args, **kwargs)
            except AjaxError, e:
                ret = {'errors': e.args}
            return ret
        return new_func
    return create_func
