#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from mwbase.views import *
from mwbase.test import TestRequest, UnitTest, no_render
from replays.models import Replay

from django.forms import ValidationError


class homeTest(UnitTest):
    def test_replay_found(self):
        #from mwbase.views import home
        Replay.objects.create(ready=True)
        replay = Replay.objects.create(ready=True)
        result = no_render(home, TestRequest())
        self.assertEquals(result['replay'].created, replay.created)
