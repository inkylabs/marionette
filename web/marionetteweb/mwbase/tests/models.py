#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from mwbase.models import *
from mwbase.test import UnitTest

from django.db.models import CharField, PositiveIntegerField


class TestModel(Model):
    objects = Manager()
    value = PositiveIntegerField(default=0)

class Model__QuerySet__incrementTest(UnitTest):
    apps = 'mwbase.tests',

    def setUp(self):
        TestModel.objects.all().delete()

    def test_increments_multiple(self):
        m1 = TestModel.objects.create(value=1)
        m2 = TestModel.objects.create(value=2)
        TestModel.objects.all().increment('value')
        self.assertEquals(TestModel.objects.get(pk=m1.pk).value, 2)
        self.assertEquals(TestModel.objects.get(pk=m2.pk).value, 3)
