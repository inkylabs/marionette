#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from mwbase.forms import *
from mwbase.test import UnitTest

from django.forms import ValidationError


class JSONField_to_pythonTest(UnitTest):
    def setUp(self):
        self.field = JSONField()

    def test_default(self):
        self.assertEquals(self.field.to_python('{}'), {})

    def test_no_json(self):
        self.assertRaises(ValidationError, self.field.to_python, '{')

    def test_passes_main_type_test(self):
        self.field.main_type = dict
        self.assertEquals(self.field.to_python('{}'), {})

    def test_fails_main_type_test(self):
        self.field.main_type = dict
        self.assertRaises(ValidationError, self.field.to_python, '[]')

    def test_list_passes_value_type_test(self):
        self.field.value_type = int
        self.assertEquals(self.field.to_python('[1]'), [1])

    def test_dict_passes_value_type_test(self):
        self.field.value_type = int
        self.assertEquals(self.field.to_python('{"key": 1}'), {'key': 1})

    def test_dict_fails_value_type_test(self):
        self.field.value_type = int
        self.assertRaises(ValidationError, self.field.to_python, '{"key": "1"}')
