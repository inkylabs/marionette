#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# There once was a blind chef named Zack,
# Whose special was flame-broiled yak.
#     How would he look
#     If it was undercooked?
# He'd bite it and it'd bite him back.
#
from mwbase.decorators import ajax_request

from annoying.decorators import render_to
from django.conf import settings
from django.http import HttpResponse


@render_to('home.html')
def home(request):
    from replays.models import Replay
    replays = Replay.objects.filter(ready=True).order_by('-created')[:1]
    replay = None
    if len(replays):
        replay = replays[0]
    return {
        'replay': replay,
    }


@ajax_request(method='GET')
def version(request):
    return {
        'min_user_version': settings.MIN_USER_VERSION,
    }
