#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from datetime import datetime, timedelta
from django.conf import settings
from django.http import Http404
from time import time


def get_day():
    return int(time() / (24 * 60 * 60))


def getint(d, key, default=None):
    if key not in d:
        return default
    try:
        return int(d[key])
    except ValueError:
        pass
    return default


def minmax(small, value, large):
    return min(max(small, value), large)


def tomorrow():
    return datetime.now() + timedelta(days=1)


def timestamp_or_404(timestamp, default=None):
    """Parse a date created with isoformat()
    """
    if not timestamp:
        return default
    fmt = '%Y-%m-%dT%H:%M:%S'
    if timestamp.find('.') >= 0:
        fmt += '.%f'
    try:
        return datetime.strptime(timestamp, fmt)
    except ValueError:
        raise Http404


def version_at_least(min_ver):
    num = lambda v: tuple(map(int, v.split('.')))
    return num(settings.VERSION) >= num(min_ver)
