#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from django.db.models.signals import post_save
from django.dispatch import Signal


__all__ = (
    'post_create',
)


post_create = Signal(providing_args=["instance", "raw", "using"])


def _post_create(sender, instance, created, **kwargs):
    if created:
        kwargs.pop('signal')
        post_create.send(sender=sender, instance=instance, **kwargs)
post_save.connect(_post_create, dispatch_uid='mwbase._post_create')
