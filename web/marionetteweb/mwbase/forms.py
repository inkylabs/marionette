#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from .errors import AjaxError

from django.forms import (Field as BaseField, Form as BaseForm,
                          ModelForm as BaseModelForm, ValidationError)
from django.utils.simplejson import loads as json_loads
from django.utils.translation import ugettext as _
from logging import getLogger


logger = getLogger(__name__)


class AjaxForm(object):
    def ajax_save(self, *args, **kwargs):
        if not self.is_valid():
            error_list = [k + ': ' + v.as_text()
                          for k, v in self.errors.items()]
            raise AjaxError(*error_list)
        return self.save()


class Form(BaseForm, AjaxForm):
    pass


class ModelForm(BaseModelForm, AjaxForm):
    pass


class JSONField(BaseField):
    default_error_messages = {
        'json': _(u'Not valid JSON.'),
        'main_type': _(u'Main type is not %s.'),
        'key_type': _(u'Key type is not %s.'),
        'value_type': _(u'Value type is not %s.'),
    }

    def __init__(self, main_type=None, key_type=None, value_type=None, *args, **kwargs):
        self.main_type = main_type
        self.key_type = key_type
        self.value_type = value_type
        super(JSONField, self).__init__(*args, **kwargs)

    def to_python(self, value):
        value = super(JSONField, self).to_python(value)
        try:
            value = json_loads(value)
        except ValueError:
            logger.error(value)
            raise ValidationError(self.error_messages['json'])
        if self.main_type and type(value) != self.main_type:
            raise ValidationError(self.error_messages['main_type'] %
                                  self.main_type.__name__)
        if self.value_type:
            iterator = value if type(value) != dict else value.values()
            for v in iterator:
                if type(v) != self.value_type:
                    raise ValidationError(self.error_messages['value_type'] %
                                          self.value_type.__name__)
        return value
