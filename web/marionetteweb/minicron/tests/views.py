#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from minicron.views import *
from mwbase.test import UnitTest, TestRequest

from datetime import datetime, timedelta


class cronTest(UnitTest):
    def setUp(self):
        Job.objects.all().delete()

    def go(self):
        cron(TestRequest())

    def register(self, name, func, freq):
        import minicron.views
        minicron.views._functions[name] = func, freq

    def test_past_job_run(self):
        run = []
        def f():
            run.append(True)
            return True
        self.register('f', f, timedelta(days=1))
        Job.objects.create(name='f', target_timestamp=datetime.now())
        self.go()
        self.assertEquals(run, [True])
        job = Job.objects.get(name='f')
        self.assertGreater(job.target_timestamp, datetime.now())

    def test_bad_job_rescheduled(self):
        self.register('f', lambda: False, timedelta(days=1))
        Job.objects.create(name='f', target_timestamp=datetime.now())
        self.go()
        job = Job.objects.get(name='f')
        self.assertLess(job.target_timestamp, datetime.now())

    def test_future_job_not_run(self):
        run = []
        def f():
            run.append(True)
            return True
        self.register('f', f, timedelta(days=1))
        target = datetime.now() + timedelta(days=1)
        Job.objects.create(name='f', target_timestamp=target)
        self.go()
        self.assertEquals(run, [])

    def test_old_job_deleted(self):
        Job.objects.create(name='none', target_timestamp=datetime.now())
        self.go()
        self.assertFalse(Job.objects.filter(name='none').exists())
