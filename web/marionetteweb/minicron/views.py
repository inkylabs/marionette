#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from .models import Job
from .registration import _functions

from datetime import datetime
from django.http import Http404, HttpResponse
from logging import getLogger


logger = getLogger(__name__)


def cron(request):
    if 'name' in request.GET:
        try:
            job = Job.objects.get(name=request.GET['name'])
        except Job.DoesNotExist:
            raise Http404
    else:
        # Get the oldest job
        ready_jobs = Job.objects.filter(target_timestamp__lte=datetime.now())
        try:
            job = ready_jobs.order_by('target_timestamp')[0]
        except IndexError:
            return HttpResponse('')

    # Run the job if it exists
    if job.name in _functions:
        func, freq = _functions[job.name]
        logger.info('running cron job "%s"' % job.name)
        result = func()
        job.target_timestamp = datetime.now()
        if result:
            job.target_timestamp += freq
        job.save()
    # Delete the job if it doesn't exist
    else:
        logger.info('deleting cron job "%s"' % job.name)
        job.delete()
    return HttpResponse('')
