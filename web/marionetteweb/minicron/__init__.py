#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from .models import Job
from .registration import _functions, register

from datetime import datetime
from logging import getLogger


logger = getLogger(__name__)


def autodiscover():
    from autoload import autodiscover as discover
    discover('cron')
    # See if we need to add any functions
    for name in _functions:
        if not Job.objects.filter(name=name).exists():
            target = datetime.now() + _functions[name][1]
            Job.objects.create(name=name, target_timestamp=target)
            logger.info('registered new cron function "%s"' % name)
