#!/bin/bash
# Install python 2.7
dir=`dirname $0`
cd $dir
dir=`pwd`
wget http://www.python.org/ftp/python/2.7.2/Python-2.7.2.tgz
tar -xf Python-2.7.2.tgz
cd Python-2.7.2
echo "431a432
>             '/usr/lib/x86_64-linux-gnu', '/usr/lib/i386-linux-gnu'," | patch setup.py
./configure --prefix $dir/env/local
make || exit
make install
cd $dir
rm -rf Python-2.7.2 Python-2.7.2.tgz

# Install virtual environment
wget http://pypi.python.org/packages/source/v/virtualenv/virtualenv-1.6.1.tar.gz
tar -xf virtualenv-1.6.1.tar.gz
cd virtualenv-1.6.1
../env/local/bin/python setup.py install
cd $dir
pwd
echo $dir
rm -rf virtualenv-1.6.1 virtualenv-1.6.1.tar.gz
env/local/bin/virtualenv \
    --python env/local/bin/python \
    --no-site-packages \
    --prompt "(web)" \
    --distribute \
    env
# This is needed to make GAE play nice with virtualenv
cd env/lib/python2.7
rm os.pyc
ln -s ../../local/lib/python2.7/os.pyc
cd $dir

# Install css files
getcss() {
  wget -O env/lib/css/$1 $2
}
mkdir env/lib/css
getcss jquery.snippet.css http://www.steamdev.com/snippet/css/jquery.snippet.css

# Install javascript files
getjs() {
  wget -O env/lib/js/$1 $2
}
mkdir env/lib/js
getjs jquery.form.js https://raw.github.com/malsup/form/63f458e575ae27d94f3889ad4a1d057ab480f596/jquery.form.js
getjs facybox.js https://raw.github.com/bitbonsai/facyBox/63c6a5073e82822998a172d347ed74525266ee6d/facybox.js
getjs jquery.snippet.js http://www.steamdev.com/snippet/js/jquery.snippet.js

# Install GAE SDK
wget http://googleappengine.googlecode.com/files/google_appengine_1.6.0.zip
unzip google_appengine_1.6.0.zip
mv google_appengine env/local/
rm -rf google_appengine_1.6.0.zip
topth() {
  echo "$@" >> env/lib/python2.7/site-packages/gae.pth
}
topth ../../../local/google_appengine
topth ../../../local/google_appengine/lib/antlr3
topth ../../../local/google_appengine/lib/fancy_urllib
topth ../../../local/google_appengine/lib/ipaddr
topth ../../../local/google_appengine/lib/webapp2
topth ../../../local/google_appengine/lib/webob
topth ../../../local/google_appengine/lib/yaml/lib

# Install some spare packages
pip="env/bin/pip install"
$pip -e hg+https://bitbucket.org/wkornewald/django-nonrel@be48c152abc6#egg=Django-dev
$pip -e hg+https://github.com/django-nonrel/nonrel-search@1290ca0f038df515818aebc92159b706c3be682b#egg=django-nonrel-search
$pip distribute==0.6.15
$pip django-annoying==0.7.6
$pip -e hg+https://bitbucket.org/twanschik/django-autoload@1698ab544030eca57be17e6d0be851ab24193762#egg=django_autoload-dev
$pip django-compress==1.0.1
$pip -e hg+https://bitbucket.org/wkornewald/django-filetransfers@32ddeacd96dc#egg=django_filetransfers-dev
$pip django-nose==0.1.3
$pip -e hg+https://bitbucket.org/wkornewald/djangoappengine@60c2b3339a9f#egg=djangoappengine-dev
$pip -e hg+https://bitbucket.org/wkornewald/djangotoolbox@baf3bbfe2541#egg=djangotoolbox
$pip ipython==0.10.2
$pip python-memcached==1.47
$pip readline==6.2.0
