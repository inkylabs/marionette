from marionette import logging
import marionette.units
import math
import random


class Toad(marionette.units.Unit):
    def __init__(self):
        self.direction = (0, 0)
        self.last_pos = (0, 0)
        self.target = None
        self.time = 0

    def dist(self, pos1, pos2):
        return math.sqrt((pos1[0] - pos2[0])**2 + (pos1[1] - pos2[1])**2)

    def dist_to_items(self):
        yield float('inf'), None
        for item in self.items_in_view:
            yield self.dist(self.pos, item.pos), item

    def calc_target(self):
        smallest_dist = self.range_of_sight
        self.target = min(self.dist_to_items())[1]

    def calc_stuck(self):
        self.stuck = self.pos == self.last_pos

    def handle_eat(self):
        if ('grutonium' in self.inventory and
            self.inventory['grutonium'] < self.max_health - self.health and
            self.inventory['grutonium'] > 0):
            return ('eat', {'typename': 'grutonium',
                            'amount': self.inventory['grutonium']})
        return False

    def handle_grab(self):
        if self.target and self.pos == self.target.pos:
            return ('grab', {'uid': self.target.uid,
                             'amount': self.target.amount})
        return False

    def handle_move(self):
        if self.target:
            self.direction = self.target.pos
        elif self.stuck:
            x = random.uniform(-1000, 1000)
            y = random.uniform(-1000, 1000)
            self.direction = (self.pos[0] + x, self.pos[1] + y)
        return 'move', {'pos': self.direction}

    def store_pos(self):
        self.last_pos = self.pos

    def act(self):
        calc = [
            self.calc_stuck,
            self.calc_target,
        ]
        for c in calc:
            c()

        handle = [
            self.handle_eat,
            self.handle_grab,
            self.handle_move,
        ]
        for h in handle:
            ret = h()
            if ret:
                break

        store = [
            self.store_pos,
        ]
        for s in store:
            s()

        self.time += 1
        return ret
