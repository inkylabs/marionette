import marionette.units
import math


class Brawlmander(marionette.units.Unit):

    def dist(self, pos1, pos2):
        return math.sqrt((pos1[0] - pos2[0])**2 + (pos1[1] - pos2[1])**2)

    def act(self):
        target = None
        min_dist = 30
        for unit in self.units_in_view:
            if unit.player == self.player:
                continue
            if self.dist(unit.pos, self.pos) < min_dist:
                target = unit.uid
                min_dist = self.dist(unit.pos, self.pos)
        if target:
            self.attack(target)
            return
        self.move(0, 0)
