import marionette.units


class Toad(marionette.units.Unit):
    def act(self):
        if self.inventory.get('grutonium', 0) >= 40 and not self.rounds_committed:
            return ('spawn', {'typename': 'brawlmander'})
        return 'move', {'pos': (0, 0)}
