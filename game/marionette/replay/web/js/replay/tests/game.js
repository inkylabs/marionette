/*
 *   Copyright 2011 Inkylabs et al.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * <Insert Marionetastical limerick here>
 *
 */
$(function() {


module('game.js : replay.Game.update');


function updateSetup() {
    var canvas_html = '<canvas id="replay" width=600 height=480></canvas>';
    $('#qunit-fixture').html(canvas_html);
    mock(replay, 'options', {
        canvasId: 'replay',
        mediaPrefix: '',
    });
    var canvas = document.createElement("canvas");
    canvas.width = 10;
    canvas.height = 10;
    animation = new jaws.Animation({
        sprite_sheet: canvas,
        frame_size: [10, 10],
    });
    var animations = {MyItem: {idle: animation, end: animation}};
    mock(replay, 'itemAnimations', animations);
    animations = {
        MyUnit: {
            0: {
                idle: animation,
                die: animation,
            },
            1: {
                idle: animation,
                die: animation,
            },
        },
    };
    mock(replay, 'unitAnimations', animations);
    mock(replay, 'backgroundAnimations', {default: {idle: animation}});
    
    mock(replay, 'isItem', function(type) {return type === 'MyItem'})
    mock(replay, 'isUnit', function(type) {return type === 'MyUnit'})
    mock(replay, 'isPlayer', function(type) {return type === 'MyPlayer'})
}


test('sprites added', function() {
    updateSetup();
    var game = new replay.Game();
    game.loadingdiv = $('<div>');
    game.rounds = [{'dimensions': [10, 10],
                    0: {type: 'MyPlayer'},
                    1: {type: 'MyPlayer'},
                    2: {type: 'MyUnit', player: 0},
                    3: {type: 'MyUnit', player: 0},
                    4: {type: 'MyItem'},
                    5: {type: 'MyItem'},
    }];
    game.viewport = replay.createViewport(
            new jaws.Rect(0, 0, 10, 10), $('#0'));
    game.update();
    ok('background' in game.sprites);
    ok(0 in game.sprites);
    ok(1 in game.sprites);
    ok(2 in game.sprites);
    ok(3 in game.sprites);
    ok(4 in game.sprites);
    ok(5 in game.sprites);
    ok('background' in game.layers[0]);
    ok(0 in game.layers[2]);
    ok(1 in game.layers[2]);
    ok(2 in game.layers[2]);
    ok(3 in game.layers[2]);
    ok(4 in game.layers[1]);
    ok(5 in game.layers[1]);
});


test('sprites deleted', function() {
    updateSetup();
    var game = new replay.Game();
    game.loadingdiv = $('<div>');
    game.rounds = [{0: {end: true}}];
    game.viewport = replay.createViewport(
            new jaws.Rect(0, 0, 10, 10), $('#0'));
    game.sprites[0] = new replay.Item({type: 'MyItem'});
    game.layers[1][0] = game.sprites[0];
    mock(game.sprites[0], 'changeEnd', function(end) {this.end = end});
    game.update();
    ok(!(0 in game.sprites))
    ok(!(0 in game.layers[1]))
});


});
