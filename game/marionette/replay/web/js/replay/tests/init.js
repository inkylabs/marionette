/*
 *   Copyright 2011 Inkylabs et al.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * <Insert Marionetastical limerick here>
 *
 */
var mocker = {};


(function() {
    var mockStore = [];


    mocker.mock = function(obj, name, value) {
        mockStore.push({
            obj: obj,
            name: name,
            value: obj[name],
            del: !(name in obj),
        });
        obj[name] = value;
    };


    mocker.unmock = function() {
        for (var i = mockStore.length - 1; i >= 0; i--) {
            var m = mockStore[i];
            m.obj[m.name] = m.value;
            if (m.del) {
                delete m.obj[m.name];
            }
        }
        mockStore = [];
    };
})();


var mock = mocker.mock;


QUnit.superReset = QUnit.reset;
QUnit.reset = function() {
    this.superReset();
    mocker.unmock();
};
