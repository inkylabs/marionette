/*
 *   Copyright 2011 Inkylabs et al.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * <Insert Marionetastical limerick here>
 *
 */
$(function() {


module('sprite.js : replay.Sprite.updateAnim');


var dummyImage = document.createElement('canvas');
dummyImage.width = 20;
dummyImage.height = 20;


test('callback made on end', function() {
    var called = false;
    var sprite = new replay.Sprite({
        anim: new jaws.Animation({
            sprite_sheet: dummyImage,
            frame_size: [20, 10],
            loop: false,
            frame_duration: -1,
        }),
        animCallback: function() {called = true;},
    });
    // FUTURE: Get rid of this line when jaws bug is fixed
    sprite.anim.loop = false;
    // Second frame shown
    sprite.updateAnim();
    ok(!called);
    // Still second frame, callback made
    sprite.updateAnim();
    ok(called);
});


});
