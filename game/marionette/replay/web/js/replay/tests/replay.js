/*
 *   Copyright 2011 Inkylabs et al.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * <Insert Marionetastical limerick here>
 *
 */
$(function() {


module('replay.js : jaws.Replay');


var canvas_html = '<canvas id="replay" width=600 height=480></canvas>';


test('jaws canvas intialized', function() {
    mock(replay, 'Game', {
        setup: function () {},
        update: function () {},
        draw: function () {
            jaws.gameloop.stop();
            start();
        },
    });
    $('#qunit-fixture').html(canvas_html);
    replay.Replay('replay', 'replay', '../../../img/');
    equal(jaws.width, 600);
    equal(jaws.height, 480);
    stop(5000);
});


});
