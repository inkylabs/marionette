/*
 *   Copyright 2011 Inkylabs et al.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 * <Insert Marionetastical limerick here>
 *
 */
$(function() {


module('unit.js : replay.Unit.updateAnim');


var dummyImage = document.createElement('canvas');
dummyImage.width = 20;
dummyImage.height = 20;


test('correct direction animation used', function() {
    var anims = [];
    for (i = 0; i < 10; i++) {
        anims.push(new jaws.Animation({
            sprite_sheet: dummyImage,
            frame_size: [20, 20],
        }));
    }
    mock(replay, 'unitAnimations', {
        Dummy: {
            0: {
                idle: anims[0],
                die: anims[1],
                moveN: anims[2],
                moveNE: anims[3],
                moveE: anims[4],
                moveSE: anims[5],
                moveS: anims[6],
                moveSW: anims[7],
                moveW: anims[8],
                moveNW: anims[9],
            }
        }
    });
    var unit = new replay.Unit({
        player: 0,
        type: 'Dummy',
    });
    unit.xPrev = 0;
    unit.yPrev = 0;

    for (var i = 0; i < 8; i++) {
        unit.xNext = 1;
        unit.yNext = Math.tan(((.25 * i) - .5) * Math.PI);
        if (i > 4) {
            unit.xNext *= -1;
            unit.yNext *= -1;
        }
        unit.updateAnim();
        equal(unit.anim, anims[i + 2]);
    }
    unit.xNext = 0;
    unit.yNext = 0;
    unit.updateAnim();
    equal(unit.anim, anims[0]);
});


});
