#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from contextlib import contextmanager
from sys import stderr


__all__ = (
    'fatality',
    'parse_int',
    'parse_loglevel',
)


@contextmanager
def fatality(err_type):
    try:
        yield
    except err_type as e:
        print(e, file=stderr)
        exit(1)


def parse_int(value):
    try:
        ret = int(value)
    except ValueError:
        print('integer value expected', file=stderr)
        exit(1)
    return ret


def parse_loglevel(level):
    all_levels = ['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL', 'NONE']
    lvl_list = 'Use one of:\n    ' + '\n    '.join(all_levels)
    newlevel = level.upper()
    if newlevel not in all_levels:
        print('"%s" is not a valid logging level.  %s' % (newlevel, lvl_list),
              file=stderr)
        exit(1)
    import logging
    if newlevel == 'NONE':
        return logging.FATAL + 1
    return getattr(logging, newlevel)
