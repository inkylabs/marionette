#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from .spatial import spawn_pos
from marionette.player import Player
from marionette.settings import get_setting
from marionette.units import get_unit_property


def can_spawn_for_level_req(unit, typename):
    """ Make sure player is of appropriate level to spawn this typename """
    if isinstance(unit.player, Player):
        level = unit.player.level
    else:
        level = get_setting('level')
    return level >= get_unit_property(typename, 'min_user_level')
