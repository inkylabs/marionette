#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from ...test import Test

from ..spatial import *


class O:
    def __init__(self, pos, size):
        self.pos = pos
        self.size = size


class nearest_pointsTest(Test):
    def test_overlapping(self):
        p1 = O((10, 10), (11, 11))
        p2 = O((20, 20), (10, 10))
        self.assertEquals(nearest_points(p1, p2), ((20, 20), (20, 20)))

    def test_diagonal(self):
        p1 = O((10, 10), (2, 2))
        p2 = O((20, 20), (2, 2))
        self.assertEquals(nearest_points(p1, p2), ((11, 11), (20, 20)))

    def test_points(self):
        self.assertEquals(nearest_points((1, 1), (2, 2)), ((1, 1), (2, 2)))


class distanceTest(Test):
    def test_overlapping(self):
        p1 = O((10, 10), (11, 11))
        p2 = O((20, 20), (10, 10))
        self.assertEquals(distance(p1, p2), 0)

    def test_s(self):
        p1 = O((10, 10), (11, 11))
        p2 = O((15, 30), (10, 10))
        self.assertEquals(distance(p1, p2), 10)

    def test_se(self):
        p1 = O((10, 10), (11, 11))
        p2 = O((23, 24), (10, 10))  # 3, 4, 5 (pythagorean triple)
        self.assertEquals(distance(p1, p2), 5)

    def test_points(self):
        self.assertEquals(distance((0, 0), (3, 4)), 5)


class overlapsTest(Test):
    def test_catches_overlap_single_corner_in(self):
        p1 = O((10, 10), (5, 5))
        p2 = O((11, 11), (5, 5))
        self.assertTrue(overlaps(p1, p2))

    def test_catches_overlap_first_rect_in_second(self):
        p1 = O((10, 10), (2, 2))
        p2 = O((5, 5), (10, 10))
        self.assertTrue(overlaps(p1, p2))

    def test_catches_overlap_second_rect_in_first(self):
        p1 = O((5, 5), (10, 10))
        p2 = O((10, 10), (2, 2))
        self.assertTrue(overlaps(p1, p2))

    def test_no_overlap(self):
        p1 = O((10, 10), (5, 5))
        p2 = O((15, 10), (5, 5))
        self.assertFalse(overlaps(p1, p2))

    def test_points_overlap(self):
        self.assertTrue(overlaps((0, 0), (0, 0)))

    def test_points_no_overlap(self):
        self.assertFalse(overlaps((0, 0), (1, 0)))


class center_posTest(Test):
    def test_center_correct(self):
        o = O((5, 5), (10, 10))
        self.assertEquals(center_pos(o), (10, 10))


class spawn_posTest(Test):
    def test_spawn_larger_than_self(self):
        from marionette.utils import spatial
        self.mockf(spatial, 'get_unit_property', (15,15))
        o = O((5, 5), (10, 10))
        self.assertEquals(spawn_pos(o, 'typename'), (2.5, 2.5))
