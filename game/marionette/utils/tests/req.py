#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from ...test import Test

from ..req import *
from marionette.player import Player



class MockPlayer(Player):
    def __init__(self, level=1):
        self.level = level


class MockUnit:
    def __init__(self, player_level=1):
        self.player = MockPlayer(player_level)


class can_spawn_for_level_reqTest(Test):
    def setUp(self):
        from marionette.utils import req
        self.mockf(req, 'get_setting', 4)
        self.mockf(req, 'get_unit_property', 4)

    def test_level_from_unit_succeeds(self):
        unit = MockUnit(5)
        self.assertTrue(can_spawn_for_level_req(unit, 'typename'))

    def test_level_from_settings_succeeds(self):
        unit = MockUnit(1)
        unit.player = 1
        self.assertTrue(can_spawn_for_level_req(unit, 'typename'))

    def test_too_low_unit_level_fails(self):
        unit = MockUnit(1)
        self.assertFalse(can_spawn_for_level_req(unit, 'typename'))

    def test_too_low_settings_level_fails(self):
        from marionette.utils import req
        self.mockf(req, 'get_setting', 1)
        unit = MockUnit(1)
        unit.player = 1
        self.assertFalse(can_spawn_for_level_req(unit, 'typename'))
