#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from os.path import expanduser
from tempfile import mkdtemp


domain = 'mutantmarionette.appspot.com'
loud = True
pref_dir = expanduser('~/.marionette')
ssl = True
tempdir = None

def store_opposite(value):
    if value:
        return 'store_false'
    return 'store_true'


def get_tempdir(prefix):
    global tempdir
    if not tempdir:
        tempdir = mkdtemp(prefix='marionette-')
    return mkdtemp(prefix=prefix, dir=tempdir)
