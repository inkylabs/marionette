#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from .. import options
from ..code import *
from ..code import UserCode
from ..test import Test, uses_tempdir

from io import StringIO
from os import makedirs
from shutil import rmtree
from subprocess import PIPE, call
from tempfile import mkdtemp
from urllib.error import URLError


class LocalCode_player_configTest(Test):
    def test_config_retrieved(self):
        from .. import code
        self.mockf(code, 'open', StringIO('{"test": "test"}'))
        user_code = UserCode()
        user_code.directory = 'test_dir'
        self.assertEquals(user_code.player_config(), {'test': 'test'})


class LocalCode_playerTest(Test):
    def test_maximal_player_created(self):
        user_code = LocalCode('testdir')
        self.mockf(user_code, 'player_config', {
            'uid': 42,
            'name': 'name',
            'upgrades': {'toad': {}},
            'level': 4,
            'new_git_url': 'git://example.git',
            'delete': True,
            'img_url': 'http://example.com/blah.jpg',
        })
        player = user_code.player()
        self.assertEquals(player.uid, 42)
        self.assertEquals(player.name, 'name')
        self.assertEquals(player.directory, 'testdir')
        self.assertEquals(player.upgrades, {'toad': {}})
        self.assertEquals(player.level, 4)
        self.assertEquals(player.new_git_url, 'git://example.git')
        self.assertEquals(player.delete, True)
        self.assertEquals(player.img_url, 'http://example.com/blah.jpg')

    def test_minimal_player_created(self):
        user_code = LocalCode('testdir')
        self.mockf(user_code, 'player_config', {
            'name': 'name',
            'level': 4,
        })
        player = user_code.player()
        self.assertEquals(player.uid, None)
        self.assertEquals(player.name, 'name')
        self.assertEquals(player.directory, 'testdir')
        self.assertEquals(player.upgrades, {})
        self.assertEquals(player.level, 4)
        self.assertEquals(player.new_git_url, None)
        self.assertEquals(player.delete, False)
        self.assertEquals(player.img_url, '')


class RemoteCode_directoryTest(Test):
    def test_correct(self):
        self.mock(options, 'pref_dir', '/blah')
        user_code = RemoteCode(42)
        self.assertEquals(user_code.directory, '/blah/code/u42')


class RemoteCode_server_configTest(Test):
    def test_returned(self):
        user_code = RemoteCode(42)
        self.mockf(user_code.client, 'get_config', {})
        self.assertEquals(user_code.server_config(), {})


class RemoteCode_urlTest(Test):
    def test_returned(self):
        user_code = RemoteCode(42)
        self.mockf(user_code.client, 'get_config',
                   {'git_url': 'git://example.git'})
        self.assertEquals(user_code.url(), 'git://example.git')


class RemoteCode_current_urlTest(Test):
    def setUp(self):
        self.mock(options, 'pref_dir', mkdtemp())
        self.user_code = RemoteCode(42)
        makedirs(self.user_code.directory)

    def tearDown(self):
        try:
            rmtree(options.pref_dir)
        except OSError:
            pass

    def test_no_directory_returns_none(self):
        rmtree(self.user_code.directory)
        self.assertEquals(self.user_code.current_url(), None)

    def test_no_git_returns_none(self):
        self.assertEquals(self.user_code.current_url(), None)

    def test_git_returns_correct(self):
        call(['git', 'init'], cwd=self.user_code.directory,
             stdout=PIPE, stderr=PIPE)
        call(['git', 'remote', 'add', 'origin', 'git://example.com/blah.git'],
             cwd=self.user_code.directory, stdout=PIPE, stderr=PIPE)
        self.assertEquals(self.user_code.current_url(),
                          'git://example.com/blah.git')


class RemoteCode_syncTest(Test):
    def setUp(self):
        self.mock(options, 'pref_dir', mkdtemp())

    def tearDown(self):
        rmtree(options.pref_dir)

    def test_sync_without_internet(self):
        user_code = RemoteCode(42)
        self.mockf(user_code, 'url', URLError)
        self.assertRaises(SyncError, user_code.sync)
