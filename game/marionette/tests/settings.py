#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from ..settings import *
from ..test import Test


class get_settingTest(Test):
    def test_setting_retreived(self):
        from .. import settings
        self.mock(settings, '_settings', {'var': 'blah'})
        self.assertEquals(get_setting('var'), 'blah')


class get_upgraded_unit_propertyTest(Test):
    def test_property_retreived(self):
        from .. import settings
        self.mock(settings, '_properties', {'mytype': {'var': 'blah'}})
        self.assertEquals(get_upgraded_unit_property('mytype', 'var'), 'blah')


class unit_definedTest(Test):
    def test_answer_correct(self):
        from .. import settings
        self.mock(settings, '_properties', {'mytype': {}})
        self.assertTrue(unit_defined('mytype'))
        self.assertFalse(unit_defined('othertype'))
