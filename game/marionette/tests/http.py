#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from ..http import *
from marionette.test import Test


class UserClient_get_version_Test(Test):
    def setUp(self):
        self.client = UserClient('')
        self.request_calls = self.mockf(self.client, 'request', {
            'min_user_version': '0.2',
        })

    def test_correct(self):
        self.client.get_min_user_version()
        self.assertEquals(self.request_calls[0][0][0], '_version')
