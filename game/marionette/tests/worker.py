#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from ..code import SyncError
from ..player import Player
from ..test import Test
from ..worker import *


class Worker_fetch_playersTest(Test):
    def test_config_retrieved(self):
        from .. import worker as mworker
        synced = self.mockf(mworker.RemoteCode, 'sync')
        parsed = self.mockf(mworker.RemoteCode, 'parse')
        self.mockf(mworker.RemoteCode, 'player_config', {
            'name': 'blah',
        })
        worker = Worker('key')
        self.mockf(worker.client, 'fetch_players', [
            {'uid': 42, 'level': 1},
        ])
        players = worker.fetch_players()

        self.assertEquals(len(players), 1)
        self.assertEquals(players[0].uid, 42)
        self.assertEquals(len(synced), 1)
        self.assertEquals(len(parsed), 1)

    def test_user_flagged(self):
        from .. import worker as mworker
        self.mockf(mworker.RemoteCode, 'sync', SyncError)
        #self.mockf(mworker.RemoteCode, 'player_config', {
            #'name': 'blah',
        #})
        worker = Worker('key')
        self.mockf(worker.client, 'fetch_players', [
            {'uid': 42, 'level': 1},
        ])
        flagged = self.mockf(worker.client, 'flag_players')
        players = worker.fetch_players()

        self.assertEquals(len(players), 0)
        self.assertEquals(len(flagged), 1)


class Worker_fetch_and_runTest(Test):
    def test_reported(self):
        from .. import worker as mworker
        self.mockf(mworker, 'mkdtemp', None)
        worker = Worker('key')
        self.mockf(worker, 'fetch_players', [Player(uid=42), Player()])
        reported = self.mockf(worker.client, 'report_results')
        def play_one_game(players, results, replay_dir):
            results[42]['xp_awarded'] += 4
        self.mock(worker, 'play_one_game', play_one_game)

        self.assertTrue(worker.fetch_and_run())
        self.assertEquals(len(reported), 1)
        self.assertEquals(len(reported[0][0]), 1)
