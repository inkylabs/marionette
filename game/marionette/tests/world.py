#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# They keep my old compasses in seven shrines
# And they refer to my strokes as "heaven lines".
#     I'm the master topographer;
#     A smooth cartographer.
# My distances are better than Levenshtein's.
#
from ..game import IDManager
from ..world import World
from ..test import Test


class World_calc_directionTest(Test):
    def setUp(self):
        self.world = World(20, 20, IDManager())

    def test_move_directly_up(self):
        direction = self.world.calc_direction(0, 2)
        self.assertEquals(direction, 'N')

    def test_move_to_cur_pos(self):
        direction = self.world.calc_direction(0, 0)
        self.assertEquals(direction, None)

    def test_move_between_N_and_NE(self):
        direction = self.world.calc_direction(1, -2)
        self.assertEquals(direction, 'NE')
