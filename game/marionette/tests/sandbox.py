#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from ..sandbox import *
from ..test import Test, uses_tempdir

# Errors occurring in a sandbox will tend to devolve into something like:
# AttributeError: 'module' object has no attribute 'stderr'
# Error in atexit._run_exitfuncs:
# TypeError: 'NoneType' object is not callable
#
# Beware!


class Sandbox_contextTest(Test):
    def test_os_not_written(self):
        # NOTE: os is a special module because this one is not simply backed
        #       up like the others
        sandbox = Sandbox()
        def write_os():
            import os
            os.x = 'x'
        with sandbox.context():
            write_os()
        import os
        self.assertFalse(hasattr(os, 'x'))

    def test_sys_stderr_exists(self):
        sandbox = Sandbox()
        def use_stderr():
            import sys
            sys.stderr
        with sandbox.context():
            use_stderr()
