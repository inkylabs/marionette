#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from ..version import *
from ..version import version_lt
from ..test import Test, uses_temp_prefs

from time import time
from urllib.error import URLError


class check_update_Test(Test):
    def go(self, **kwargs):
        from .. import prefs
        self.mockf(prefs, 'prefs', kwargs)
        check_update()

    def setUp(self):
        from .. import version
        self.set_calls = self.mockf(version, 'set_prefs')
        self.print_calls = self.mockf(version, 'print')

    def test_prompts_user(self):
        from .. import version
        input_call = self.mockf(version, 'input', 'n')
        from .. import http
        self.mockf(http.UserClient, 'get_min_user_version', '0.0')
        self.go()
        self.assertEquals(len(input_call), 1)
        self.assertEquals(len(self.print_calls), 0)
        kwargs = self.set_calls[0][1]
        self.assertFalse(kwargs['allow_update_check'])

    def test_version_old(self):
        from .. import http
        self.mockf(http.UserClient, 'get_min_user_version', '100.0')
        before = time()
        self.go(allow_update_check=True)
        self.assertEquals(len(self.print_calls), 1)
        kwargs = self.set_calls[0][1]
        self.assertGreater(kwargs['last_update_check'], before)
        self.assertLess(kwargs['last_update_check'], time())

    def test_no_network(self):
        from .. import http
        self.mockf(http.UserClient, 'get_min_user_version', URLError)
        self.go(allow_update_check=True)
        self.assertEquals(len(self.print_calls), 0)
        self.assertEquals(len(self.set_calls), 0)


def version_is_validTest(Test):
    def test_valid_version_passes(self):
        self.assertTrue(version_is_valid('23.2.100'))
    def test_version_invalid_fails(self):
        self.assertFalse(version_is_valid('3a,4'))


def version_lt(Test):
    def test_lt_valid_version_passes(self):
        self.assertTrue(version_lt('1', '1.2'))
        self.assertTrue(version_lt('1.2', '1.3'))
        self.assertFalse(version_lt('1.2', '1.2'))
        self.assertFalse(version_lt('1.3', '1.2'))
