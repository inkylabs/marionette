#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from ..collision import *

from math import sqrt
from random import seed as random_seed
from unittest import TestCase


class CollisionDetector__pathTest(TestCase):
    def setUp(self):
        random_seed(0)
        self.cd = CollisionDetector((20, 20), (10, 10))

    def test_path_straight(self):
        i = self.cd._path((0, 1), (0, -1))
        self.assertEquals(list(i), [
            (0, 0),
            (0, -1),
        ])

    def test_path_diagonal(self):
        path = list(self.cd._path((0, 0), (2, 2)))
        s = 1 / sqrt(2)
        self.assertEquals(len(path), 3)
        self.assertAlmostEquals(path[0][0], s)
        self.assertAlmostEquals(path[1][0], 2 * s)
        self.assertAlmostEquals(path[2][0], 2)


class CollisionDetector_get_posTest(TestCase):
    def setUp(self):
        random_seed(0)
        self.cd = CollisionDetector((20, 20), (10, 10))

    def test_no_collision_ignored(self):
        self.cd.add((11, 11), (2, 2))
        pos = self.cd.get_pos((5, 5), (9, 9), (2, 2))
        self.assertEquals(pos, (9, 9))

    def test_collision_detected(self):
        self.cd.add((10, 10), (2, 2))
        pos = self.cd.get_pos((5, 5), (9, 9), (2, 2))
        self.assertEquals(pos, (8.0, 8.0))

    def test_jump_right(self):
        #    0    5    0    5    0
        #     ____________________
        #  0 |                    |
        #    |                    |
        #    |                    |
        #    |                    |
        #    |                    |
        #  5 |         x          |
        #    |         x          |
        #    |         x          |
        #    |         x          |
        #    |         x          |
        # 10 |    sss  x    eee   |
        #    |    sss  x    eee   |
        #    |    sss  x    eee   |
        #    |         x          |
        #    |         x          |
        # 15 |         x          |
        #    |                    |
        #    |                    |
        #    |                    |
        #    |                    |
        # 20 |____________________|
        self.cd.add((10, 5), (1, 10))
        pos = self.cd.get_pos((5, 10), (15, 10), (3, 3))
        self.assertEquals(pos, (7, 10))

    def test_jump_left(self):
        #    0    5    0    5    0
        #     ____________________
        #  0 |                    |
        #    |                    |
        #    |                    |
        #    |                    |
        #    |                    |
        #  5 |         x          |
        #    |         x          |
        #    |         x          |
        #    |         x          |
        #    |         x          |
        # 10 |    eee  x    sss   |
        #    |    eee  x    sss   |
        #    |    eee  x    sss   |
        #    |         x          |
        #    |         x          |
        # 15 |         x          |
        #    |                    |
        #    |                    |
        #    |                    |
        #    |                    |
        # 20 |____________________|
        self.cd.add((10, 5), (1, 10))
        pos = self.cd.get_pos((15, 10), (5, 10), (3, 3))
        self.assertEquals(pos, (11, 10))

    def test_jump_right_down(self):
        self.cd.add((10, 5), (1, 10))
        pos = self.cd.get_pos((4, 8), (10, 16), (3, 3))
        self.assertAlmostEquals(pos[0], 7)
        self.assertAlmostEquals(pos[1], 12)

    def test_jump_up(self):
        self.cd.add((0, 10), (20, 1))
        pos = self.cd.get_pos((10, 15), (10, 5), (3, 3))
        self.assertEquals(pos, (10, 11))

    def test_jump_down(self):
        self.cd.add((0, 10), (20, 1))
        pos = self.cd.get_pos((10, 5), (10, 15), (3, 3))
        self.assertEquals(pos, (10, 7))


class CollisionDetector_pos_is_validTest(TestCase):
    def setUp(self):
        self.cd = CollisionDetector((20, 20), (10, 10))
        self.cd.add((5, 5), (10, 10))

    def test_catches_overlap_bottom_right_corner(self):
        self.assertFalse(self.cd.pos_is_valid((4, 4), (2, 2)))

    def test_catches_overlap_top_left_corner(self):
        self.assertFalse(self.cd.pos_is_valid((14, 14), (2, 2)))

    def test_catches_overlap_inside(self):
        self.assertFalse(self.cd.pos_is_valid((10, 10), (2, 2)))

    def test_no_overlap_passes(self):
        self.assertTrue(self.cd.pos_is_valid((2, 2), (2, 2)))


class CollisionDetector_addTest(TestCase):
    def setUp(self):
        self.cd = CollisionDetector((20, 20), (10, 10))

    def test_add_item_size_of_world(self):
        self.cd.add((0, 0), (20, 20))


class CollisionDetector_collisionTest(TestCase):
    def setUp(self):
        self.cd = CollisionDetector((20, 20), (10, 10))
        self.cd.add((0, 0), (20, 1))
        self.cd.add((0, 0), (1, 20))
        self.cd.add((0, 19), (20, 1))
        self.cd.add((19, 0), (1, 20))

    def test_collide_with_top_left(self):
        end_pos = self.cd.get_pos((3, 3), (-10, -10), (1, 1))
        self.assertEquals(end_pos, (1, 1))

    def test_collide_with_bottom_right(self):
        end_pos = self.cd.get_pos((15, 15), (22, 22), (1, 1))
        self.assertEquals(end_pos, (18, 18))

    def test_point_in_world_body_out_of_world(self):
        end_pos = self.cd.get_pos((10, 10), (15, 15), (6, 6))


class CollisionDetector__calc_tilesTest(TestCase):
    def setUp(self):
        self.cd = CollisionDetector((20, 20), (10, 10))

    def test_calc_tiles_gives_tiles_in_world(self):
        tiles = self.cd._calc_tiles((10, 10), (15, 15), (6, 6))
        ret = []
        for i in tiles:
            ret.append(i)
        self.assertEquals(ret, [[]])
