#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from ..items import Item
from ..game import *
from ..player import Player
from ..replay import Round
from marionette.test import Test
from marionette.units.sandboxed_unit import SandboxedUnit
from marionette.units.unit import Unit

from collections import Counter


mockitem = {
    'typename': 'mockitem',
    'size': [20, 20],
    'weight': 1,
}


# Like SandboxedUnit, but more light-weight
# (Doesn't create a subprocess)
class TestUnit:
    def __init__(self, typename='typename', player=None, uid=1):
        self.typename = typename
        self.player = player
        self.uid = uid
        self.pos = (100, 100)
        self.size = [20, 20]
        self.items_in_view = []
        self.units_in_view = []
        self.obstacles_in_view = []
        self.effects = {}
        self.inventory = {}
        self.inventory_weight = 0.0
        self.max_inventory_weight = 100.0
        self.health = 100
        self.max_health = 100
        self.range_of_sight = 10
        self.running_speed = 10
        self.melee_strength = 10
        self.attack_time_req = .2
        self.spawn_items_req = {'typename': {'grutonium': 25}}
        self.spawn_time_req = {'typename': 1}
        self.rounds_committed = None
        self.action_data = 'idle', {}
        self._is_alive = True

    def is_alive(self):
        return self.is_alive

    def act(self, queue=None):
        if queue:
            queue.put(self)

    def kill(self):
        self._is_alive = False


class TestGame(Game):
    def __init__(self):
        super(TestGame, self).__init__(900, 600, write_replay=False)
        self.curround = Round()

    def make_player(self):
        uid = self.uids.generate()
        self.players[uid] = Player(uid=uid)
        return self.players[uid]

    def make_unit(self, **kwargs):
        if not self.players:
            self.make_player()
        player = list(self.players.values())[0]
        uid = self.uids.generate()
        unit = TestUnit(uid=uid, player=player, **kwargs)
        self.world.units[uid] = unit
        player.units[uid] = unit
        return unit


class Game_run_roundTest(Test):
    def setUp(self):
        self.game = TestGame()
        self.unit = self.game.make_unit()

    def test_rounds_committed_decremented(self):
        self.unit.rounds_committed = 10
        self.game.run_round()
        self.assertEquals(self.unit.rounds_committed, 9)


class Game_unit_attackTest(Test):
    def setUp(self):
        self.game = TestGame()
        self.unit1 = self.game.make_unit()
        self.unit2 = self.game.make_unit()

    def test_begin_attack(self):
        self.game.unit_attack(self.unit1, self.unit2.uid)
        self.assertEquals(self.unit1.rounds_committed, 2)
        change = {self.unit1.uid: {'intent': 'attackS'}}
        self.assertEquals(self.game.curround, change)

    def test_attack_nonexistant_target(self):
        self.game.unit_attack(self.unit1, 12345)
        self.assertEquals(self.unit1.rounds_committed, None)

    def test_currently_waiting(self):
        self.unit1.rounds_committed = 1
        self.game.unit_attack(self.unit1, self.unit2.uid)
        self.assertEquals(self.unit1.rounds_committed, 1)
        self.assertEquals(self.unit2.health, 100)

    def test_end_attack(self):
        self.unit1.rounds_committed = 0
        self.game.unit_attack(self.unit1, self.unit2.uid)
        self.assertEquals(self.unit2.health, 90)

    def test_not_overlapping(self):
        self.unit2.pos = (10, 10)
        self.unit1.rounds_committed = 0
        self.game.unit_attack(self.unit1, self.unit2.uid)
        self.assertEquals(self.unit2.health, 100)


class Game_unit_dieTest(Test):
    def setUp(self):
        self.game = TestGame()
        self.player = self.game.make_player()
        self.unit = self.game.make_unit()
        self.another_unit = self.game.make_unit()
        self.unit.inventory = {'grutonium': 10}

    def test_removed_from_game_and_player(self):
        self.game.unit_die(self.unit)
        self.assertEquals(len(self.game.world.units), 1)
        self.assertFalse(self.unit.uid in self.player.units)

    def test_unit_with_no_inventory_raises_no_error(self):
        del self.unit.inventory
        self.game.unit_die(self.unit)


class Game_unit_dropTest(Test):
    def setUp(self):
        self.game = TestGame()
        self.unit = self.game.make_unit()
        self.unit.inventory['grutonium'] = 10

    def test_drop_valid_amount(self):
        self.game.unit_drop(self.unit, 'grutonium', 5)
        # Make sure unit has less grutonium
        self.assertEquals(self.unit.inventory['grutonium'], 5)
        # Make sure the item appears in the world
        self.assertEquals(len(self.game.world.items), 1)
        item = [i for i in self.game.world.items.values()][0]
        self.assertEquals(item.typename, 'grutonium')
        self.assertEquals(item.amount, 5)

    def test_drop_more_than_have(self):
        self.game.unit_drop(self.unit, 'grutonium', 15)
        self.assertFalse('grutonium' in self.unit.inventory)
        self.assertEquals(len(self.game.world.items), 1)
        item = [i for i in self.game.world.items.values()][0]
        self.assertEquals(item.typename, 'grutonium')
        self.assertEquals(item.amount, 10)


class Game_unit_eatTest(Test):
    def setUp(self):
        self.game = TestGame()
        self.unit = self.game.make_unit()
        self.unit.inventory['grutonium'] = 10
        self.unit.metabolism = {'grutonium': {'health': 1}}

    def test_eat_successfully(self):
        self.unit.health = 90
        self.game.unit_eat(self.unit, 'grutonium', 5)
        self.assertEquals(self.unit.inventory['grutonium'], 5)
        self.assertEquals(self.unit.health, 95)

    def test_eat_more_than_have(self):
        self.unit.health = 80
        self.game.unit_eat(self.unit, 'grutonium', 20)
        self.assertEquals(self.unit.health, 90)
        self.assertFalse('grutonium' in self.unit.inventory)

    def test_eat_over_max_health(self):
        self.unit.health = 95
        self.game.unit_eat(self.unit, 'grutonium', 10)
        self.assertEquals(self.unit.health, 100)


class Game_unit_grabTest(Test):
    def setUp(self):
        self.game = TestGame()
        self.unit = self.game.make_unit()
        self.unit.size = [30, 30]
        self.unit.inventory['grutonium'] = 5

        uid = self.game.uids.generate()
        self.gru = Item('grutonium', self.unit.pos, 5, uid)
        self.game.world.items[uid] = self.gru

    def tearDown(self):
        self.unit.kill()

    def test_grab_success(self):
        self.game.unit_grab(self.unit, self.gru.uid, 5)
        self.assertEquals(self.unit.inventory['grutonium'], 10)
        self.assertEquals(self.game.world.items, {})

    def test_out_of_range_fails(self):
        self.gru.pos = (self.gru.pos[0] + 100, self.gru.pos[1] + 100)
        self.game.unit_grab(self.unit, self.gru.uid, 5)
        self.assertEquals(self.unit.inventory['grutonium'], 5)

    def test_too_large_fails(self):
        self.unit.size = [10, 10]
        self.game.unit_grab(self.unit, self.gru.uid, 5)
        self.assertEquals(self.unit.inventory['grutonium'], 5)


class Game_unit_moveTest(Test):
    def setUp(self):
        self.game = TestGame()
        self.unit = self.game.make_unit()

    def tearDown(self):
        self.unit.kill()

    def test_move_successfully(self):
        self.game.unit_move(self.unit, (104, 103))
        self.assertEquals(self.unit.pos, (104, 103))
        change = {self.unit.uid: {
            'intent': 'moveSE',
            'pos': (104, 103),
        }}
        self.assertEquals(self.game.curround, change)

    def test_move_off_map(self):
        self.unit.pos = (4, 4)
        self.game.unit_move(self.unit, (-5, -5))
        self.assertEquals(self.unit.pos, (1, 1))

    def test_move_out_of_range(self):
        self.unit.running_speed = 5
        self.game.unit_move(self.unit, (106, 108))
        self.assertEquals(self.unit.pos, (103, 104))

    def test_move_in_negative_direction(self):
        self.unit.running_speed = 5
        self.game.unit_move(self.unit, (94, 92))
        self.assertEquals(self.unit.pos, (97, 96))


class Game_unit_spawnTest(Test):
    def setUp(self):
        self.game = TestGame()
        self.unit = self.game.make_unit()
        self.unit.inventory['grutonium'] = 100
        self.unit_defs = {'typename': {
            'min_user_level': 1,
            'size': [30, 30]
        }}
        from ..units import defs
        self.mock(defs, '_defs', self.unit_defs)
        self.added = self.mockf(self.game, 'add_unit')

    def test_start_spawn(self):
        self.game.unit_spawn(self.unit, 'typename')
        self.assertEquals(self.unit.inventory['grutonium'], 75)
        self.assertEquals(self.unit.rounds_committed, 10)
        change = {self.unit.uid: {
            'intent': 'spawn',
            'inventory': {'grutonium': 75},
        }}
        self.assertEquals(self.game.curround, change)

    def test_level_too_low_fails(self):
        self.unit_defs['typename']['min_user_level'] = 10
        self.game.unit_spawn(self.unit, 'typename')
        self.assertEquals(self.unit.rounds_committed, None)

    def test_currently_committed(self):
        self.unit.rounds_committed = 2
        self.game.unit_spawn(self.unit, 'typename')
        self.assertEquals(self.unit.inventory['grutonium'],  100)
        self.assertEquals(self.unit.rounds_committed, 2)

    def test_finish_spawn(self):
        self.unit.rounds_committed = 0
        self.game.unit_spawn(self.unit, 'typename')
        self.assertEquals(len(self.added), 1)
        self.assertEquals(self.added[0][0][2], (95, 95))

    def test_not_enough_grutonium(self):
        self.unit.inventory['grutonium'] = 10
        self.game.unit_spawn(self.unit, 'typename')
        self.assertEquals(self.unit.rounds_committed, None)

    def test_not_enough_space(self):
        self.unit_defs['typename']['size'] = [1000, 1000]
        from ..units import defs
        self.mock(defs, '_defs', self.unit_defs)
        self.game.unit_spawn(self.unit, 'typename')
        self.assertEquals(self.unit.rounds_committed, None)
