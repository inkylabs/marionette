#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from ..prefs import *
from ..prefs import prefs
from ..test import Test, uses_temp_prefs

from os.path import join as path_join


class prefs_Test(Test):
    def mock_prefs(self, value):
        from .. import prefs as mprefs
        self.mock(mprefs, '_prefs', value)

    @uses_temp_prefs
    def test_prefs_exists(self):
        _prefs = {'test': 'test'}
        self.mock_prefs(_prefs)
        self.assertEquals(prefs(), _prefs)

    @uses_temp_prefs
    def test_no_file(self):
        self.mock_prefs(None)
        self.assertEquals(prefs(), {})

    @uses_temp_prefs
    def test_bad_json(self):
        self.mock_prefs(None)
        from .. import options
        f = open(path_join(options.pref_dir, 'prefs.json'), 'w')
        f.write('blah')
        f.close()
        self.assertEquals(prefs(), {})


class set_prefs_Test(Test):
    def test_prefs_initiated(self):
        from .. import prefs as mprefs
        mprefs._prefs = None
        from io import StringIO
        sio = StringIO()
        self.mockf(mprefs, 'open', sio)
        self.mockf(mprefs, 'json_load', {'start': 'value'})
        set_prefs(pref='test')
        self.assertEquals(prefs(), {'pref': 'test', 'start': 'value'})
