#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from ...test import Test
from ...items import Item
from ..unit import *


class verify_actionTest(Test):
    def go(self):
        verify_action((self.action, self.data), 'myunit')

    def setUp(self):
        from .. import unit
        self.mock(unit, '_action_params', {'myaction': {
            'uid',
            'typename',
            'amount',
            'pos',
        }})
        self.mockf(unit, 'get_available_actions', {'myaction'})
        self.action = 'myaction'
        self.data = {
            'uid': 1,
            'typename': 'toad',
            'amount': 1,
            'pos': [1, 1.5],
        }

    def test_valid_action_passes(self):
        self.go()

    def test_invalid_data_fails(self):
        self.data = None
        self.assertRaises(ParamError, self.go)

    def test_nonstr_name_fails(self):
        self.action = None
        self.assertRaises(ParamError, self.go)

    def test_invalid_name_fails(self):
        self.action = 'otheraction'
        self.assertRaises(ParamError, self.go)

    def test_invalid_data_type_fails(self):
        self.data = None
        self.assertRaises(ParamError, self.go)

    def test_invalid_data_key_type_fails(self):
        self.data[0] = 1
        self.assertRaises(ParamError, self.go)

    def test_invalid_params_fails(self):
        self.data['blah'] = 1
        self.assertRaises(ParamError, self.go)

    def test_nontuple_pos_fails(self):
        self.data['pos'] = 1
        self.assertRaises(ParamError, self.go)

    def test_3tuple_pos_fails(self):
        self.data['pos'] = (1, 2, 3)
        self.assertRaises(ParamError, self.go)

    def test_nonfloat_pos_fails(self):
        self.data['pos'] = ('a', 1)
        self.assertRaises(ParamError, self.go)

    def test_nonint_posint_fails(self):
        self.data['posint'] = 1.5
        self.assertRaises(ParamError, self.go)

    def test_neg_posint_fails(self):
        self.data['posint'] = -1
        self.assertRaises(ParamError, self.go)

    def test_nonstr_str_fails(self):
        self.data['str'] = 1
        self.assertRaises(ParamError, self.go)


class copy_valuesTest(Test):
    def test_attributes_copied(self):
        item = Item(typename='grutonium', pos=(10, 10), amount=5, uid=3)
        unit = Unit()
        setattr(unit, 'typename', 'toad')
        setattr(unit, 'pos', (20, 20))
        setattr(unit, 'health', 50)
        setattr(unit, 'max_health', 100)
        setattr(unit, 'uid', 4)
        class MockPlayer:
            uid = 5
        unit.player = MockPlayer
        d1 = {
            'origin': (10, 10),
            'items_in_view': [item],
            'units_in_view': [unit],
        }
        d2 = {
        }
        copy_values(d1, d2, ['items_in_view', 'units_in_view'])

        # items_in_view
        self.assertTrue('items_in_view' in d2)
        self.assertEquals(len(d2['items_in_view']), 1)
        item = d2['items_in_view'][0]
        self.assertEquals(item.typename, 'grutonium')
        self.assertEquals(item.pos, (20, 20))
        self.assertEquals(item.amount, 5)
        self.assertEquals(item.uid, 3)

        # units_in_view
        self.assertTrue('units_in_view' in d2)
        self.assertEquals(len(d2['units_in_view']), 1)
        unit = d2['units_in_view'][0]
        self.assertEquals(unit.typename, 'toad')
        self.assertEquals(unit.pos, (30, 30))
        self.assertEquals(unit.health, 50)
        self.assertEquals(unit.max_health, 100)
        self.assertEquals(unit.uid, 4)
        self.assertEquals(unit.player, 5)


class resolve_attributesTest(Test):
    def test_upgrades_correct(self):
        from .. import unit
        self.mockf(unit, 'get_base_properties', {'prop1': 1, 'prop2': 0})
        self.mockf(unit, 'get_unit_upgrades', {
            'attr2': [{'prop2': 0}, {'prop2': 1}, {'prop2': 2}],
            'attr3': [{'prop3': 0}, {'prop3': 1}, {'prop3': 2}, {'prop3': 3}],
        })
        attributes = resolve_attributes('myunit', {'attr2': 2, 'attr3': 3})
        self.assertEquals(attributes, {
            'prop1': 1,
            'prop2': 2,
            'prop3': 3,
        })
