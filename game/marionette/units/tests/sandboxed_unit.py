#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from ...player import Player
from ...test import Test
from ..sandboxed_unit import *

from multiprocessing import Manager
from queue import Queue
from threading import Semaphore, Thread
from time import sleep, time
from types import MethodType


class SandboxedUnitProcess___init__Test(Test):
    def test_variables_initialized(self):
        p = Player(0)
        semaphore = Semaphore()
        manager = Manager()
        proc = SandboxedUnitProcess('toad', p, origin=(1, 2), uid=3,
                                    manager=manager, semaphore=semaphore)
        self.assertEquals(proc.daemon, True)


class SandboxedUnitProcess_startTest(Test):
    def test_starts(self):
        p = self.create_idling_player()
        semaphore = Semaphore()
        manager = Manager()
        proc = SandboxedUnitProcess('toad', p, origin=(1, 2), uid=3,
            manager=manager, semaphore=semaphore)
        self.logged = self.mockf(proc, 'log')
        proc.start()
        proc._started.wait(2.0)
        self.assertTrue(proc._started.is_set())
        self.assertIsNotNone(proc.pid)
        self.assertWaitTrue(lambda: proc._lock.acquire(False))
        self.assertEquals(len(self.logged), 0)
        proc.kill()


class SandboxedUnitProcess__doTest(Test):
    def setUp(self):
        self.mock(SandboxedUnitProcess, 'run', lambda x: sleep(10))
        self.proc = SandboxedUnitProcess('toad', Player(0),
                                         manager=Manager(), semaphore=Semaphore())
        self.logged = self.mockf(self.proc.player, 'log')
        super(SandboxedUnitProcess, self.proc).start()
        self.proc._started.set()
        self.proc.max_time = 1.0
        self.proc.max_memory = 0.5
        self.proc._start_memory = 2 ** 32

    def tearDown(self):
        self.proc.kill()

    def test_message_sent(self):
        Thread(target=self.proc._do, args=('test', ['a'], {'b': 'c'})).start()
        self.assertTrue(self.proc._child_pipe.poll(1.0))
        msg = self.proc._child_pipe.recv()
        self.proc._child_pipe.send({})
        self.assertEquals(len(msg), 3)
        self.assertEquals(msg[0], 'test')
        self.assertEquals(msg[1], ['a'])
        self.assertEquals(msg[2], {'b': 'c'})

    def test_time_limited(self):
        self.proc.max_time = 0.01
        margs = self.mockf(self.proc, 'suspend')
        thread = Thread(target=self.proc._do, args=('test',))
        thread.start()
        self.proc._child_pipe.recv()
        self.assertWaitEquals(lambda: len(margs), 1, timeout=0.02)
        thread.join()
        self.assertEquals(len(self.logged), 1)
        self.assertEquals(self.logged[0][0][0][:9], 'TimeError')

    def test_memory_limited(self):
        self.proc._start_memory = 0
        thread = Thread(target=self.proc._do, args=('test',))
        thread.start()
        self.proc._child_pipe.send({})
        self.assertWaitFalse(lambda: self.proc.is_alive())
        thread.join()
        self.assertEquals(len(self.logged), 1)
        self.assertEquals(self.logged[0][0][0][:11], 'MemoryError')


class SandboxedUnitProcess__lock_doTest(Test):
    def setUp(self):
        self.mock(SandboxedUnitProcess, 'run', lambda x: sleep(10))
        self.doargs = self.mockf(SandboxedUnitProcess, '_do')
        self.proc = SandboxedUnitProcess('toad', Player(0),
                                         manager=Manager(),
                                         semaphore=Semaphore())
        super(SandboxedUnitProcess, self.proc).start()
        self.proc._started.set()

    def tearDown(self):
        self.proc.kill()

    def test_wait_on_locks(self):
        pass
        # We need to wait on the locks in order
        self.proc._lock.acquire()
        queue = Queue()
        Thread(target=self.proc._lock_do, args=('test',),
               kwargs={'queue': queue, 'resume': False}).start()
        self.assertFalse(self.proc._child_pipe.poll())
        self.assertTrue(self.proc._semaphore.acquire(False))
        # Give the first lock
        self.proc._lock.release()
        self.assertEquals(len(self.doargs), 0)
        # Give the second lock
        self.proc._semaphore.release()
        self.assertWaitEquals(lambda: len(self.doargs), 1)
        # Wait for the locks to be released
        self.assertWaitTrue(lambda: self.proc._semaphore.acquire(False))
        self.assertWaitTrue(lambda: self.proc._lock.acquire(False))
        # Make sure the process was added to the queue
        self.assertEquals(queue.get(True, 0.1), self.proc)
        # Make sure _do got run
        self.assertEquals(len(self.doargs), 1)

    def test_exit_if_dead(self):
        self.proc.kill()
        self.assertWaitFalse(lambda: self.proc.is_alive())
        self.proc._lock_do('test', resume=False)
        self.assertEquals(len(self.doargs), 0)


class SandboxedUnitProcess_runTest(Test):
    def go(self, method):
        self.player = self.create_idling_player()
        self.player.upgrades = {'toad': {}}
        self.player.settings = {'upgrades': self.player.upgrades}
        self.semaphore = Semaphore()
        self.proc = SandboxedUnitProcess('toad', self.player, origin=(1, 2),
                                         uid=3, manager=Manager(),
                                         semaphore=self.semaphore)
        from ..sandboxed_unit import sandboxed
        @sandboxed
        def f(s):
            return method()
        self.proc._test = MethodType(f, self.proc)
        self.proc.start()
        self.proc._lock.acquire()
        self.proc.resume()
        self.proc._parent_pipe.send(['test', [], {}])

    def tearDown(self):
        self.proc.kill()

    def test_handles_none(self):
        self.go(lambda: {})
        self.assertTrue(self.proc._parent_pipe.poll(2.0))
        self.assertEquals(self.proc._parent_pipe.recv(), {})
        self.assertTrue(self.proc.is_alive())
        self.assertEquals(len(self.proc._err_data), 0,
                          msg=str(self.proc._err_data))

    def test_handles_messages(self):
        self.go(lambda: {1: 2})
        self.assertTrue(self.proc._parent_pipe.poll(2.0))
        self.assertEquals(self.proc._parent_pipe.recv(), {1: 2})
        self.assertEquals(len(self.proc._err_data), 0,
                          msg=str(self.proc._err_data))

    def test_handles_errors(self):
        self.go(lambda: int(None))
        self.assertTrue(self.proc._parent_pipe.poll(2.0))
        self.assertEquals(self.proc._parent_pipe.recv(), {})
        self.assertEquals(len(self.proc._err_data), 1,
                          msg=str(self.proc._err_data))

    def test_modules_setup(self):
        def get_values():
            ret = []
            import marionette.logging
            ret.append(str(marionette.logging._log_data))
            import marionette.settings
            ret.append(marionette.settings._settings)
            ret.append(marionette.settings._properties)
            return ret
        self.go(get_values)
        values = self.proc._parent_pipe.recv()
        print(values)
        self.assertEquals(len(self.proc._err_data), 0,
                          msg=str(self.proc._err_data))
        self.assertEquals(len(values), 3)
        self.assertEquals(values[0], str(self.proc._log_data))
        self.assertEquals(values[1], self.proc.player.settings)
        self.assertEquals(len(values[2]), 1)


class SandboxedUnitProcess__startTest(Test):
    def setUp(self):
        import sys
        self.pathlen = len(sys.path)

    def tearDown(self):
        import sys
        if len(sys.path) > self.pathlen:
            sys.path.pop()

    def test_unit_initialized(self):
        p = self.create_player_with_code([
            'import marionette.units',
            'class Toad(marionette.units.Unit):',
            '    def __init__(self):',
            '        self.copyid = self.uid',
        ])
        p.set_sys_path()
        proc = SandboxedUnitProcess('toad', p, origin=(1, 2), uid=3,
                                    manager=Manager())
        self.assertIsNone(proc._start())
        self.assertEquals(proc.unit.uid, 3)
        self.assertEquals(proc.unit.copyid, 3)
        proc._start_time = time()
        self.assertGreater(proc.unit.used_time, 0)
        self.assertLess(proc.unit.used_time, .1)
        proc._start_memory = 0
        self.assertGreater(proc.unit.used_memory, 0)


class SandboxedUnitProcess__actTest(Test):
    def setUp(self):
        import sys
        self.pathlen = len(sys.path)

    def tearDown(self):
        import sys
        if len(sys.path) > self.pathlen:
            sys.path.pop()

    def test_unit_acts(self):
        p = self.create_player_with_code([
            'import marionette.units',
            'class Toad(marionette.units.Unit):',
            '    def act(self):',
            '        return "move", {"pos": (0, 0)}',
        ])
        proc = SandboxedUnitProcess('toad', p, origin=(1, 2), uid=3,
                                    manager=Manager())
        proc.origin = (1, 1)
        class TestUnit:
            def act(self):
                return 'move', {'pos': (0, 0)}
        proc.unit = TestUnit()
        proc._act()
        self.assertEquals(proc._action_data[0], ('move', {'pos': (-1, -1)}))
