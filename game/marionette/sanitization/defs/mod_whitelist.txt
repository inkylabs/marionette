# self
.

# std
cmath
math
random

# marioneette
marionette.game.time_per_round
marionette.items.Item
marionette.items.get_item_property
marionette.logging.debug
marionette.logging.info
marionette.logging.warning
marionette.logging.error
marionette.logging.critical
marionette.settings
marionette.units.Unit
marionette.units.get_unit_property
marionette.utils
