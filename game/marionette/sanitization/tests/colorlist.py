#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from ..colorlist import *
from unittest import TestCase


class subattrsTest(TestCase):
    def test_absolute_correct(self):
        i = subattrs('a.b.c')
        self.assertEquals(i.__next__(), 'a')
        self.assertEquals(i.__next__(), 'a.b')
        self.assertEquals(i.__next__(), 'a.b.c')
        self.assertRaises(StopIteration, i.__next__)

    def test_relative_correct(self):
        i = subattrs('..a.b.c')
        self.assertEquals(i.__next__(), '..')
        self.assertEquals(i.__next__(), '..a')
        self.assertEquals(i.__next__(), '..a.b')
        self.assertEquals(i.__next__(), '..a.b.c')
        self.assertRaises(StopIteration, i.__next__)

    def test_only_relative_correct(self):
        i = subattrs('...')
        self.assertEquals(i.__next__(), '...')
        self.assertRaises(StopIteration, i.__next__)


class attr_in_colorlistTest(TestCase):
    def go(self, attr, w={}):
        return attr_in_colorlist(attr, w)

    def test_root_attr_in_whitelist(self):
        self.assertTrue(self.go('a.b.c', w={'a'}))

    def test_branch_attr_in_whitelist(self):
        self.assertTrue(self.go('a.b.c', w={'a.b'}))

    def test_leaf_attr_in_whitelist(self):
        self.assertTrue(self.go('a.b.c', w={'a.b.c'}))

    def test_further_attr_in_whitelist(self):
        self.assertFalse(self.go('a.b.c', w={'a.b.c.d'}))

    def test_attr_not_in_whitelist(self):
        self.assertFalse(self.go('a.b.c'))

    def test_relative_attr_in_whitelist(self):
        self.assertTrue(self.go('.a', w={'.'}))

    def test_higher_attr_not_in_whitelist(self):
        self.assertFalse(self.go('..a', w={'.a'}))

    def test_pure_higher_attr_not_in_whitelist(self):
        self.assertFalse(self.go('..', w={'.'}))

    def test_pure_higher_attr_in_whitelist(self):
        self.assertFalse(self.go('.', w={'..'}))


class prefixesTest(TestCase):
    def test_correct(self):
        i = prefixes(['a.b', 'c'])
        self.assertEquals(i.__next__(), 'a')
        self.assertEquals(i.__next__(), 'a.b')
        self.assertEquals(i.__next__(), 'c')
        self.assertRaises(StopIteration, i.__next__)
