#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from ...test import Test
from ..parser import ParseError
from ..validation import *

from json import dumps as json_dumps
from os import remove
from os.path import join as path_join
from shutil import rmtree
from tempfile import mkdtemp


class parse_licenseTest(Test):
    def test_valid_license(self):
        class newsha1:
            def digest():
                return (b'+\x8b\x81R)\xaa\x8aa\xe4\x83' +
                        b'\xfbK\xa0X\x8b\x8blI\x18\x90')
        from .. import validation
        self.mockf(validation, 'sha1', newsha1)
        self.assertEquals(parse_license(''), [])

    def test_invalid_license(self):
        self.assertRaises(ParseError, parse_license, '')


class parse_settingsTest(Test):
    def go_upgrades(self, updates={}):
        settings = {
            'name': 'player',
            'level': 1,
            'api_version': '0.2',
        }
        settings.update(updates)
        parse_settings(json_dumps(settings))

    def test_upgrades_nondict_fails(self):
        self.assertRaises(ParseError, self.go_upgrades, {'upgrades': []})

    def test_upgrades_invalid_unittype_fails(self):
        self.assertRaises(ParseError, self.go_upgrades, {
            'upgrades': {'blah': {}}
        })

    def test_upgrades_nonsubdict_fails(self):
        self.assertRaises(ParseError, self.go_upgrades, {
            'upgrades': {'toad': []}
        })

    def test_upgrades_invalid_upgradetype_fails(self):
        self.assertRaises(ParseError, self.go_upgrades, {
            'upgrades': {'toad': {'blah': 0}}
        })

    def test_upgrades_invalid_level_fails(self):
        self.assertRaises(ParseError, self.go_upgrades, {
            'upgrades': {'toad': {'defense': -1}}
        })

    def test_upgrades_unavailable_unittype_fails(self):
        self.assertRaises(ParseError, self.go_upgrades, {
            'upgrades': {'brawlmander': {'defense': 1}}
        })

    def test_upgrades_correct(self):
        self.go_upgrades({'toad': {'defense': 0}})

    def test_version_too_low(self):
        from marionette import version
        self.mock(version, 'version_num', '0.2')
        self.assertRaises(ParseError, self.go_upgrades, {
            'upgrades': {'toad': {'defense': 0}},
            'api_version': '0.1'
        })


class parse_pyTest(Test):
    def test_valid_py_file(self):
        self.assertEquals(parse_py(''), [])

    def test_invalid_py_file(self):
        self.assertRaises(ParseError, parse_py, 'invalid python')


class parse_fileTest(Test):
    def setUp(self):
        self.temp_dir = mkdtemp()

    def tearDown(self):
        rmtree(self.temp_dir)

    def go(self, filename, funcname):
        from .. import validation
        self.calls = self.mockf(validation, funcname, [])
        self.path = path_join(self.temp_dir, filename)
        f = open(self.path, 'w')
        f.close()
        return parse_file(self.path)

    def test_innocuous_file_not_opened(self):
        self.assertEquals(parse_file('blah.pyc'), [])

    def test_license_handled(self):
        self.assertEquals(self.go('LICENSE', 'parse_license'), [])
        self.assertEquals(len(self.calls), 1)

    def test_settings_handled(self):
        self.assertEquals(self.go('settings.json', 'parse_settings'), [])
        self.assertEquals(len(self.calls), 1)

    def test_py_handled(self):
        self.assertEquals(self.go('blah.py', 'parse_py'), [])
        self.assertEquals(len(self.calls), 1)


class parse_dirTest(Test):
    def setUp(self):
        self.temp_dir = mkdtemp()
        class newsha1:
            def digest():
                return (b'+\x8b\x81R)\xaa\x8aa\xe4\x83' +
                        b'\xfbK\xa0X\x8b\x8blI\x18\x90')
        from .. import validation
        from marionette import version
        self.mock(version, 'version_num', '0.2')
        self.mockf(validation, 'sha1', newsha1)
        f = open(path_join(self.temp_dir, 'LICENSE'), 'w')
        f.close()
        f = open(path_join(self.temp_dir, 'settings.json'), 'w')
        f.write('{"name": "sarah", "level": 1, "api_version": "0.2"}')
        f.close()

    def tearDown(self):
        rmtree(self.temp_dir)

    def test_has_license(self):
        remove(path_join(self.temp_dir, 'LICENSE'))
        self.assertRaises(ParseError, parse_dir, self.temp_dir)

    def test_no_settings(self):
        remove(path_join(self.temp_dir, 'settings.json'))
        self.assertRaises(ParseError, parse_dir, self.temp_dir)

    def test_valid(self):
        f = open(path_join(self.temp_dir, '__init__.py'), 'w')
        f.close()
        parse_dir(self.temp_dir)
        self.assertTrue(True)

    def test_invalid_py(self):
        f = open(path_join(self.temp_dir, 'file.py'), 'w')
        f.write('invalid python')
        f.close()
        self.assertRaises(ParseError, parse_dir, self.temp_dir)
