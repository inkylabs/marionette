#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
from ..colorlist import prefixes
from ..parser import *
from ast import parse as ast_parse
from unittest import TestCase


class parse_individual_attrsTest(TestCase):
    def go(self, code):
        return parse_individual_attrs(ast_parse(code).body[0].value)

    def test_attrs_parsed(self):
        i = self.go('a.b.c')
        self.assertEquals(i.__next__(), 'c')
        self.assertEquals(i.__next__(), 'b')
        self.assertEquals(i.__next__(), 'a')
        self.assertRaises(StopIteration, i.__next__)

    def test_none_parsed(self):
        i = self.go('a()')
        self.assertRaises(StopIteration, i.__next__)


class parse_attrTest(TestCase):
    def go(self, code):
        return parse_attr(ast_parse(code).body[0].value)

    def test_name_parsed(self):
        self.assertEquals(self.go('a'), 'a')

    def test_attr_parsed(self):
        self.assertEquals(self.go('a.b.c'), 'a.b.c')

    def test_none_parsed(self):
        self.assertEquals(self.go('a()'), '')


class parse_importTest(TestCase):
    def go(self, code):
        return dict(parse_import(ast_parse(code).body[0]))

    def test_standard_import(self):
        self.assertEquals(self.go('import a'), {'a': ['a']})

    def test_import_multiple(self):
        self.assertEquals(self.go('import a, b'), {'a': ['a'], 'b': ['b']})

    def test_import_attr(self):
        self.assertEquals(self.go('import a.b'), {'a.b': ['a.b']})

    def test_import_alias(self):
        self.assertEquals(self.go('import a.b as c'), {'c': ['a.b']})

    def test_import_from_module(self):
        self.assertEquals(self.go('from a import b'), {'b': ['a.b']})

    def test_import_multiple_from_module(self):
        self.assertEquals(self.go('from a import b, c'),
                          {'b': ['a.b'], 'c': ['a.c']})

    def test_import_alias_from_module(self):
        self.assertEquals(self.go('from a import b as c'), {'c': ['a.b']})

    def test_import_star_from_whitelisted_module(self):
        self.assertEquals(self.go('from a import *'), {'*': ['a.*']})

    def test_import_multiple_aliases(self):
        self.assertEquals(self.go('import a as c, b as c'), {'c': ['a', 'b']})

    def test_import_one_dot(self):
        self.assertEquals(self.go('from . import a'), {'a': ['.a']})

    def test_import_one_dot_module(self):
        self.assertEquals(self.go('from .a import b'), {'b': ['.a.b']})

    def test_import_two_dots(self):
        self.assertEquals(self.go('from .. import a'), {'a': ['..a']})

    def test_import_two_dots_module(self):
        self.assertEquals(self.go('from ..a import b'), {'b': ['..a.b']})


class is_nonrestricted_moduleTest(TestCase):
    def go(self, attr, w={}):
        return is_nonrestricted_module(attr, w, set(prefixes(w)))

    def test_whitelisted_module_accepted(self):
        self.assertTrue(self.go('a.b', w={'a'}))

    def test_expandable_to_whitelist_module_accepted(self):
        self.assertTrue(self.go('a', w={'a.b'}))

    def test_restricted_module_rejected(self):
        self.assertFalse(self.go('a', w={'b'}))

    def test_nonwhitelisted_relative_module_rejected(self):
        self.assertFalse(self.go('.a', w={'a'}))

    def test_whitelisted_relative_module_accepted(self):
        self.assertTrue(self.go('.a', w={'.a'}))

    def test_whitelisted_pwd_module_accepted(self):
        self.assertTrue(self.go('.a', w={'.'}))

    def test_nonwhitelisted_higher_with_whitelisted_pwd_module_rejected(self):
        self.assertFalse(self.go('..a', w={'.'}))


class parse_importsTest(TestCase):
    def go(self, code, w={}):
        return dict(parse_imports(ast_parse(code), w, set(prefixes(w))))

    def test_totally_whitelisted_import_accepted(self):
        self.assertEquals(self.go('import a', w={'a'}), {'a': ['a']})

    def test_partially_whitelisted_import_accepted(self):
        self.assertEquals(self.go('import a', w={'a.b'}), {'a': ['a']})

    def test_whitelisted_import_attr_accepted(self):
        self.assertEquals(self.go('import a.b', w={'a'}), {'a.b': ['a.b']})

    def test_nonwhitelisted_import_rejected(self):
        self.assertRaises(ParseError, self.go, 'import a')

    def test_divergent_nonwhitelisted_import_rejected(self):
        self.assertRaises(ParseError, self.go, 'import a.b', w={'a.c'})

    def test_import_star_from_whitelisted_module(self):
        self.assertEquals(self.go('from a import *', w={'a'}), {})

    def test_import_star_from_module_with_whitelisted_expansion(self):
        self.assertRaises(ParseError, self.go, 'from a import *', w={'a.b'})

    def test_import_multiple_aliases_multple_lines(self):
        code = 'import a as c\nimport b as c'
        self.assertEquals(self.go(code, w={'a', 'b'}), {'c': ['a', 'b']})


class test_alias_expansionsTest(TestCase):
    def test_expansions_correct(self):
        i = alias_expansions('a.b.c', {'a': ['d', 'e'], 'a.b': ['f', 'g']})
        self.assertEquals(i.__next__(), 'd.b.c')
        self.assertEquals(i.__next__(), 'e.b.c')
        self.assertEquals(i.__next__(), 'f.c')
        self.assertEquals(i.__next__(), 'g.c')
        self.assertRaises(StopIteration, i.__next__)


class is_whitelisted_expansionTest(TestCase):
    def go(self, attr, w={}, t={}):
        return is_whitelisted_expansion(attr, w, t)

    def test_bad_leaf_rejected(self):
        self.assertFalse(self.go('a.b', ['a.c'], {'a': ['a']}))

    def test_good_alias_accepted(self):
        self.assertTrue(self.go('a.b', ['c.b'], {'a': ['c']}))

    def test_bad_alias_rejected(self):
        self.assertFalse(self.go('a.b', ['a.b'], {'a': ['c']}))

    def test_bad_prefix_rejected(self):
        self.assertFalse(self.go('a', ['a.b'], {'a': ['a']}))

    def test_bad_second_alias_rejected(self):
        self.assertFalse(self.go('a', ['a'], {'a': ['a', 'c']}))


class parse_assignmentTest(TestCase):
    def go(self, code, g={}, w={}, t={}):
        return parse_assignment(ast_parse(code).body[0].value, g, w, t)

    def test_nonattr_accepted(self):
        self.assertTrue(parse_assignment(ast_parse('')))

    def test_nongraylisted_kw_accepted(self):
        self.assertTrue(self.go('a'))

    def test_whitelisted_expansion_accepted(self):
        self.assertTrue(self.go('a', w={'a'}, t={'a': ['a']}))

    def test_graylisted_kw_rejected(self):
        self.assertFalse(self.go('a', g={'a'}))

    def test_nonwhitelisted_expansion_rejected(self):
        self.assertFalse(self.go('a', w={'a.b'}, t={'a': ['a']}))

    def test_graylisted_attr_rejected(self):
        self.assertFalse(self.go('a.b', g={'b'}))


class parse_assignmentsTest(TestCase):
    def go(self, code, g={}, w={}, t={}):
        root = ast_parse(code)
        return parse_assignments(ast_parse(code), g, w, t)

    # Test a few special conditions for imports
    def test_whitelisted_import_assignment_accepted(self):
        self.assertTrue(self.go('a = b', w={'b'}, t={'b': ['b']}))

    def test_nonwhitelisted_import_assignment_rejected(self):
        self.assertRaises(ParseError, self.go, 'a = b', w={'b.c'},
                          t={'b': ['b']})

    def test_nonwhitelisted_aliased_import_assignment_rejected(self):
        self.assertRaises(ParseError, self.go, 'a = b', w={'b'},
                          t={'b': ['c']})

    # All other tests are for keywords
    def test_nongraylisted_kw_assignment_accepted(self):
        self.assertTrue(self.go('a = b'))

    def test_graylisted_kw_assignment_rejected(self):
        self.assertRaises(ParseError, self.go, 'a = b', g={'b'})

    def test_graylisted_kw_middle_rejected(self):
        self.assertRaises(ParseError, self.go, 'a = b.c.d', g={'c'})

    def test_graylisted_kw_list_wrap_rejected(self):
        self.assertRaises(ParseError, self.go, '[a]', g={'a'})

    def test_graylisted_kw_tuple_wrap_rejected(self):
        self.assertRaises(ParseError, self.go, '(a,)', g={'a'})

    def test_graylisted_kw_set_wrap_rejected(self):
        self.assertRaises(ParseError, self.go, '{a}', g={'a'})

    def test_graylisted_kw_dict_key_wrap_rejected(self):
        self.assertRaises(ParseError, self.go, '{a: b}', g={'a'})

    def test_graylisted_kw_dict_value_wrap_rejected(self):
        self.assertRaises(ParseError, self.go, '{a: b}', g={'b'})

    def test_graylisted_kw_parameter_rejected(self):
        self.assertRaises(ParseError, self.go, 'a(b)', g={'b'})

    def test_graylisted_kw_base_class_rejected(self):
        self.assertRaises(ParseError, self.go, 'class A(B):\n    pass',
                          g={'B'})

    def test_graylisted_kw_default_parameter_rejected(self):
        self.assertRaises(ParseError, self.go, 'def a(b=c):\n    pass',
                          g={'c'})

    def test_graylisted_kw_return_rejected(self):
        self.assertRaises(ParseError, self.go, 'return a', g={'a'})

    def test_graylisted_kw_yield_rejected(self):
        self.assertRaises(ParseError, self.go, 'yield a', g={'a'})


class is_nonrestricted_expansionTest(TestCase):
    def go(self, attr, w={}, t={}):
        return is_nonrestricted_expansion(attr, w, set(prefixes(w)), t)

    def test_nonrestricted_expansion_accepted(self):
        self.assertTrue(self.go('a', w={'a'}))

    def test_restricted_expansion_rjected(self):
        self.assertFalse(self.go('a', w={'a'}, t={'a': ['b']}))


class parse_loadTest(TestCase):
    def go(self, code, b={}, w={}, t={}):
        return parse_load(ast_parse(code).body[0].value, b, w,
                          set(prefixes(w)), t)

    def test_nonattr_accepted(self):
        self.assertTrue(parse_load(ast_parse('')))

    def test_whitelisted_expansion_accepted(self):
        self.assertTrue(self.go('a', w={'a.b'}, t={'a': ['a']}))

    def test_nonblacklisted_attr_accepted(self):
        self.assertTrue(self.go('a'))

    def test_restricted_expansion_rejected(self):
        self.assertFalse(self.go('a.b', w={'a.c'}, t={'a': ['a']}))

    def test_blacklisted_kw_rejected(self):
        self.assertFalse(self.go('a', b={'a'}))

    def test_blacklisted_kw_attr_rejected(self):
        self.assertFalse(self.go('a.b', b={'b'}))


class parse_loadsTest(TestCase):
    def go(self, code, b={}, w={}, t={}):
        return parse_loads(ast_parse(code), b, w, set(prefixes(w)),
                           t)

    def test_clean_load_accepted(self):
        self.assertTrue(self.go('a'))

    def test_restricted_module_rejected(self):
        self.assertRaises(ParseError, self.go, 'a.b', w={'a.c'},
                          t={'a': ['a']})

    def test_blacklisted_kw_rejected(self):
        self.assertRaises(ParseError, self.go, 'a', b={'a'})

    def test_blacklisted_kw_attr_rejected(self):
        self.assertRaises(ParseError, self.go, 'a.b', b={'b'})


class parseTest(TestCase):
    def go(self, code, b={}, g={}, w={}):
        return parse(code, b, g, w, set(prefixes(w)))

    def test_clean_code_accepted(self):
        self.assertTrue(self.go('a = 1 + 2'))

    # (0) Generate syntax errors
    def test_bad_syntax_rejected(self):
        self.assertRaises(ParseError, self.go, 'a a')

    # (1) Import an attr that has no whitelisted expansion
    def test_restricted_import_rejected(self):
        self.assertRaises(ParseError, self.go, 'import a')

    # (2) Incorrectly use a protected symbol
    def test_incorrect_use_of_protected_symbol_rejected(self):
        self.assertRaises(ParseError, self.go, 'a = b', g={'b'})

    # (3) Use a restricted symbol
    def test_incorrect_use_of_restricted_symbol_rejected(self):
        self.assertRaises(ParseError, self.go, 'a', b={'a'})
