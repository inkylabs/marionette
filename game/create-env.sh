#!/bin/bash
#
#   Copyright 2011 Inkylabs et al.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# <Insert Marionetastical limerick here>
#
dir=`dirname $0`
cd $dir

# Python3
virtualenv -p python3 \
    --no-site-packages \
    --distribute env \
    --prompt "(game)"

pip="env/bin/pip install"
$pip nose==1.1.2


# Download the javascript we need
tpjsdir=marionette/replay/web/js/third_party
mkdir $tpjsdir
mkdir $tpjsdir/test
getjs() {
  wget -O $tpjsdir/$1 $2
}
getjs jquery.js http://code.jquery.com/jquery-1.5.1.js
getjs jquery.dotimeout.js https://github.com/cowboy/jquery-dotimeout/raw/v1.0/jquery.ba-dotimeout.js
getjs jaws.js https://raw.github.com/ippa/jaws/1fda1025872d04d74bc971623154883bbb1fc713/jaws.js
getjs test/qunit.js https://raw.github.com/jquery/qunit/7f292170fa1109f1355f3e96f8973c32fc553946/qunit/qunit.js
getjs test/qunit.css https://raw.github.com/jquery/qunit/7f292170fa1109f1355f3e96f8973c32fc553946/qunit/qunit.css
