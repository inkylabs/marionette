#!/bin/bash

PORT=29418
#USER=puppetmaster
USER=cco3
HOST=inkylabs.com


function read_json {
    readjsonpy="
import json
data = json.loads('$1')
attrs = []
"
    for attr in ${@:2}
    do
        attr=${attr//./\'][\'}
        readjsonpy="$readjsonpy
attrs.append(str(data['$attr']))"
    done
    readjsonpy="$readjsonpy
print(' '.join(attrs))"
    python -c "$readjsonpy"
}


function get_patchinfo {
    ssh -p $PORT $USER@$HOST gerrit query \
        --format JSON \
        --current-patch-set \
        status:open | \
        head -n -1 |
        while read line
        do
            read_json "$line" lastUpdated currentPatchSet.number
            echo "$line"
        done
}


function handle {
    timestamp=$1
    patchnum=$2
    approval=$3

    # Make sure we haven't already reviewed this
    if [ $approval = -2 ]
    then
        return 0
    fi

    # Make sure the change can merge
    origsha=$(git rev-parse HEAD)
    git pull ssh://$USER@$HOST:$PORT/marionette \
        refs/changes/$changemod/$changenum/$patchnum
    message=
    verified=-1
    ret=1
    if [ $origsha != $(git rev-parse HEAD) ]
    then
        message="Change didn't merge.  Fix things up, fool."
    fi

    # Make sure we have correct ownership

    # Make sure the presubmit tests pass

    # Reset and send results to gerrit
    git reset --hard $origsha
    ssh -p $PORT $USER@$HOST gerrit review --message $message
    return $ret
}


while true
do
    git pull
    noop=1
    get_patchinfo | sort -n | \
        while read line
        do
            echo "$line"
            if [ handle $line ]
            then
                git pull
                noop = 0
            fi
        done
    if [ $noop ]
    then
        sleep 20
    fi
done
